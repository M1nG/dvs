/**
 *
 * @param id
 * @param option
 * @param charts
 Menu*/
// 渲染echarts
function setPushChartsArr(id, option, charts) {
    pushChartsArr.forEach((item, index) => {
        if (item.id === id) {
            item.option = JSON.parse(JSON.stringify(option))
            item.charts = charts
            // pushChMenuartsArr[index].splice(index,Menu1)
        }
    })
}

//例子
/*var 标准气泡图 = {
    xAxis: {
        series: "",
        name: "星期",
        data: ['周一', '周二', '周三', '周四', '周五']
    },
    yAxis: {
        series: [
            {name: '最高人气', data: [9000, 8000, 7000, 6000, 8888]},
            {name: '最低人气', data: [2000, 3000, 2500, 3500, 4500]}
        ],
        name: "数值",
        data: ""
    }
}*/

let chartsOptions = [
    {
        class: 'left1',
        option: {
            title: {//标题
                show: true,// 是否显示
                text: '标准气泡图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['气泡图'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            xAxis: {
                show: true,//是否显示X轴
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线
                },
                axisTick: {
                    show: true,//是否显示刻度
                },
                data: ['周一', '周二', '周三', '周四', '周五']
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                }
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            series: [{
                itemStyle: {
                    color: '#f5af19'
                },
                name: '最高人气',
                symbolSize: 20,//气泡大小
                data: [9000, 8000, 7000, 6000, 8888],
                type: 'scatter',
            }, {
                name: '最低人气',
                itemStyle: {
                    color: '#f12711'
                },
                symbolSize: 20,
                data: [2000, 3000, 2500, 3500, 4500],
                type: 'scatter'
            }],
            animation: true,
            animationEasing: 'elasticOut',
        },
        showMenu: [1, 2, 3, 5, 6, 7,  15, 27, 28, 30,],
    }, {
        class: 'left2',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准折线图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气', '最低人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            xAxis: {
                show: true,//是否显示X轴
                data: ['周一', '周二', '周三', '周四', '周五'],
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true//是否显示分割线
                },
                axisTick: {
                    show: true//是否显示刻度
                },
                type: 'category'
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                },
                type: 'value',
            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            series: [{
                data: [820, 932, 901, 934, 1290],
                type: 'line',
                name: '最高人气',
                symbolSize: 10,//拐点大小
                lineStyle: {
                    color: '#f12711',//折线颜色
                    type: 'solid',//折线类型
                    width: 2,//线宽
                },
                smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                // areaStyle: {
                //     normal: {}
                // },
                itemStyle: {
                    borderColor: '#f12711',//拐点描边颜色
                    borderWidth: 1,//拐点宽度

                },
            }, {
                data: [320, 432, 301, 234, 590],
                type: 'line',
                name: '最低人气',
                symbolSize: 10,//拐点大小
                lineStyle: {
                    color: '#f5af19',//折线颜色
                    type: 'solid',//折线类型
                    width: 2,//线宽
                },
                smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                // areaStyle: {
                //     normal: {}
                // },
                itemStyle: {
                    borderColor: '#f5af19',//拐点描边颜色
                    borderWidth: 1,//拐点宽度

                },
            }]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 9, 27, 28, 30,]
    }, {
        class: 'left3',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准面积图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气', '最低人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                formatter: '{b}天：<br/>{c}',
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: 'red',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            xAxis: {
                show: true,//是否显示X轴
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                },
                type: 'category',
                data: ['周一', '周二', '周三', '周四', '周五'],
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                },
                type: 'value',
            },
            backgroundColor: 'transparent',//背景颜色
            //偏移值
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },

            series: [
                {
                    type: 'line',
                    //曲线弯度
                    name: '最高人气',
                    symbolSize: 10,//拐点大小
                    lineStyle: {
                        color: '#f12711',//折线颜色
                        type: 'solid',//折线类型
                        width: 2,//线宽
                    },
                    smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                    areaStyle: {
                        normal: {}
                    },
                    itemStyle: {
                        borderColor: '#f12711',//拐点描边颜色
                        borderWidth: 1,//拐点宽度

                    },
                    data: [129, 210, 98, 229, 218],
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            color: '#fff',
                            fontSize: 9,
                        }
                    }
                }, {
                    type: 'line',
                    //曲线弯度
                    name: '最低人气',
                    areaStyle: {
                        normal: {}
                    },
                    data: [109, 410, 198, 129, 318],
                    symbolSize: 10,//拐点大小
                    lineStyle: {
                        color: '#f5af19',//折线颜色
                        type: 'solid',//折线类型
                        width: 2,//线宽
                    },
                    smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                    // areaStyle: {
                    //     normal: {}
                    // },
                    itemStyle: {
                        borderColor: '#f5af19',//拐点描边颜色
                        borderWidth: 1,//拐点宽度

                    },
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            color: '#fff',
                            fontSize: 9,
                        }
                    }
                }
            ]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 9, 15, 27, 28, 30,]
    }, {
        class: 'left4',
        option: {
            title: {
                show: false,// 是否显示
                text: '水平折线图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气', '最低人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                formatter: '{b}：<br/>{c}',
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: 'red',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            xAxis: {
                show: true,//是否显示X轴
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                },
                type: 'value',
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                },
                type: 'category',
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                containLabel: false
            },
            series: [{
                data: [820, 932, 901, 934, 1290, 1330, 1320],
                type: 'line',
                name: '最高人气',
                symbolSize: 10,//拐点大小
                lineStyle: {
                    color: '#f12711',//折线颜色
                    type: 'solid',//折线类型
                    width: 2,//线宽
                },
                smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                // areaStyle: {
                //     normal: {}
                // },
                itemStyle: {
                    borderColor: '#f12711',//拐点描边颜色
                    borderWidth: 1,//拐点宽度

                },
            }, {
                data: [320, 432, 401, 434, 890, 630, 720],
                type: 'line',
                name: '最低人气',
                symbolSize: 10,//拐点大小
                lineStyle: {
                    color: '#f5af19',//折线颜色
                    type: 'solid',//折线类型
                    width: 2,//线宽
                },
                smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                // areaStyle: {
                //     normal: {}
                // },
                itemStyle: {
                    borderColor: '#f5af19',//拐点描边颜色
                    borderWidth: 1,//拐点宽度

                },
            }]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 15, 27, 28, 30,]
    }, {
        class: 'left5',
        option: {
            title: {
                show: false,// 是否显示
                text: '折柱混合图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小
                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['蒸发量', '平均温度'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'cross',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            backgroundColor: 'transparent',//背景颜色
            xAxis: [
                {
                    show: true,//是否显示X轴
                    axisLine: {
                        show: true,//是否显示X轴线
                        lineStyle: {
                            color: '#eee',//轴线颜色
                            width: 1, //这里是为了突出显示加上的
                        },
                    },
                    name: '时间',//X轴名称
                    max: 'dataMax',//X轴最大值
                    min: 'dataMin',//X轴最小值
                    inverse: false,//是否反向坐标轴
                    axisLabel: {
                        fontSize: 14,//字体大小
                        rotate: 0,//坐标轴旋转角度
                        color: '#eee',//标签颜色
                        interval: 0,//坐标轴分隔线的显示间隔
                    },
                    splitLine: {
                        show: false,//是否显示分割线

                    },
                    axisTick: {
                        show: false,//是否显示刻度
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
                },
            ],
            yAxis: [
                {
                    show: true,//是否显示Y轴
                    axisLine: {
                        show: true,//是否显示Y轴线
                        lineStyle: {
                            color: '#eee',//轴线颜色
                            width: 1, //这里是为了突出显示加上的
                        },
                    },
                    name: '数值',//Y轴名称
                    max: 'dataMax',//Y轴最大值
                    min: 'dataMin',//Y轴最小值
                    inverse: false,//是否反向坐标轴
                    axisLabel: {
                        fontSize: 14,//字体大小
                        rotate: 0,//坐标轴旋转角度
                        color: '#eee',//标签颜色
                        formatter: '{value} W',
                        interval: 0,//坐标轴分隔线的显示间隔
                    },
                    splitLine: {
                        show: false,//是否显示分割线

                    },
                    axisTick: {
                        show: false,//是否显示刻度
                    },
                    type: 'value',
                    axisPointer: {
                        snap: true
                    },
                },
                {
                    show: false,//是否显示Y轴
                    axisLine: {
                        show: true,//是否显示Y轴线
                        lineStyle: {
                            color: '#eee',//轴线颜色
                            width: 1, //这里是为了突出显示加上的
                        },
                    },
                    name: '数值',//Y轴名称
                    max: 'dataMax',//Y轴最大值
                    min: 'dataMin',//Y轴最小值
                    inverse: false,//是否反向坐标轴
                    axisLabel: {
                        fontSize: 14,//字体大小
                        rotate: 0,//坐标轴旋转角度
                        color: '#eee',//标签颜色
                        formatter: '{value} W',
                        interval: 0,//坐标轴分隔线的显示间隔
                    },
                    splitLine: {
                        show: false,//是否显示分割线

                    },
                    axisTick: {
                        show: false,//是否显示刻度
                    },
                    type: 'value',
                    axisPointer: {
                        snap: true
                    },
                }
            ],
            series: [
                {
                    name: '蒸发量',
                    type: 'bar',
                    data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                    itemStyle: {
                        color: '#f12711'
                    },
                },

                {
                    name: '平均温度',
                    type: 'line',
                    yAxisIndex: 1,
                    data: [2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2],
                    symbolSize: 10,//拐点大小
                    itemStyle: {
                        borderColor: '#f5af19',//拐点描边颜色
                        borderWidth: 1,//拐点宽度
                    },
                    lineStyle: {
                        color: '#f5af19',//折线颜色
                        type: 'solid',//折线类型
                        width: 2,//线宽
                    },
                }
            ]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 15, 27, 28, 30,]
    }, {
        class: 'left6',
        option: {
            title: {
                show: false,// 是否显示
                text: '关系折线图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['流量', '降雨量'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                    animation: false
                },
            },
            grid: [{
                bottom: '55%',
                left: '5%',
                right: '5%',
                height: '35%',
                top: '5%',
                containLabel: false,
            }, {
                left: '5%',
                right: 10,
                top: '55%',
                height: '35%',
                bottom: '5%',
                containLabel: false,
            }],
            xAxis: [{
                show: true,//是否显示X轴
                type: 'category',
                boundaryGap: false,
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                }
            }, {
                show: true,//是否显示X轴
                gridIndex: 1,
                type: 'category',
                boundaryGap: true,
                // axisLine: {onZero: true},
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                position: 'top',
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                }
            }],
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: [{
                show: true,//是否显示Y轴
                type: 'value',
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                }
            }, {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                gridIndex: 1,
                type: 'value',
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: true,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: true,//是否显示刻度
                }
            }],
            backgroundColor: 'transparent',//背景颜色
            series: [
                {
                    name: '流量',
                    type: 'line',
                    symbolSize: 10,
                    itemStyle: {
                        borderColor: '#f12711',//拐点描边颜色
                        borderWidth: 1,//拐点宽度
                    },
                    lineStyle: {
                        color: '#f12711',//折线颜色
                        type: 'solid',//折线类型
                        width: 2,//线宽
                    },
                    smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                    data: [820, 932, 901, 934, 1290, 1330, 1320]
                },
                {
                    name: '降雨量',
                    type: 'line',
                    itemStyle: {
                        borderColor: '#f5af19',//拐点描边颜色
                        borderWidth: 1,//拐点宽度
                    },
                    lineStyle: {
                        color: '#f5af19',//折线颜色
                        type: 'solid',//折线类型
                        width: 2,//线宽
                    },
                    smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
                    xAxisIndex: 1,
                    yAxisIndex: 1,
                    symbolSize: 10,
                    data: [820, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        },
        showMenu: [1, 2, 3, 4, 7, 9, 27, 28, 30,]
    }, {
        class: 'left7',
        option: {
            title: {
                show: false,// 是否显示
                text: '两点折线图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: 'rgba(0,0,0,0.5)',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            xAxis: {
                show: true,//是否显示X轴
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值1',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值2',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: true,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            series: [{
                data: [[20, 120], [50, 200], [40, 50]],
                name: '最高人气',
                type: 'line',
                symbolSize: 10,//拐点大小
                itemStyle: {
                    borderColor: '#f5af19',//拐点描边颜色
                    borderWidth: 1,//拐点宽度
                },
                lineStyle: {
                    color: '#f5af19',//折线颜色
                    type: 'solid',//折线类型
                    width: 2,//线宽
                },
                smooth: 0,//取值范围 0 到 1，表示平滑程度，越小表示越接近折线段
            }]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 9, 27, 28, 30,]
    }, {
        class: 'left8',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准柱状图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: 'rgba(0,0,0,0.5)',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'cross',//指示器类型
                    lineStyle: {
                        color: '#EEE',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            xAxis: {
                show: true,//是否显示X轴
                type: 'category',
                data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                type: 'value',
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            },
            backgroundColor: 'transparent',//背景颜色

            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            series: [{
                data: [120, 200, 150, 80, 70, 110, 130],
                type: 'bar',
                name: '最高人气',
                showBackground: true,
                barGap: '30%',
                barWidth: 30,
                itemStyle: {
                    color: '#f5af19',
                    barBorderRadius: 0,
                },

            }]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 8, 27, 28, 30,]
    }, {
        class: 'left9',
        option: {
            // 标题
            title: {
                show: false,// 是否显示
                text: '水平柱状图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                top: 'top',//标题垂直对齐方式
                left: 'left',//标题水平对齐方式
                textStyle: {
                    color: '#0deae0',//标题颜色
                    fontSize: 16,//标题字体大小
                    fontWeight: 'normal',

                },

            },
            legend: {//图例
                show: false,//是否显示
                data: ['最高人气'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'cross',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            // 偏移值
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            xAxis: {
                show: false,//是否显示X轴
                type: 'value',
                splitNumber: 2,
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            },
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: {
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '名称',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    show: true,
                    interval: 0,
                },
                splitLine: {
                    show: false,//是否显示分割线
                },
                axisTick: {
                    show: false,//是否显示刻度
                },
                type: 'category',
                data: ['半山', '上塘', '湖墅', '小河', '拱宸桥',]
            },

            backgroundColor: 'transparent',//背景颜色
            series: [{
                data: [75, 81, 87, 91, 95],
                name: '最高人气',
                type: 'bar',
                // 柱子后面显示的文字
                label: {
                    normal: {
                        show: true,
                        position: 'right',
                        formatter: '{c}%'
                    },

                },
                // 自定义柱子颜色
                itemStyle: {
                    color: '#f5af19',
                    barBorderRadius: 0,
                },
                // 柱子宽度
                barWidth: 30,
                barGap: '30%',
            }]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 8, 27, 28, 30,]
    }, {
        class: 'left10',
        option: {
            title: {
                show: false,// 是否显示
                text: '垂直柱状堆叠图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['谷歌', '百度'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'shadow',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            xAxis: [{
                show: true,//是否显示X轴
                type: 'category',
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            }],
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            backgroundColor: 'transparent',//背景颜色
            yAxis: [{
                show: true,//是否显示Y轴
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                type: 'value',
                name: '数值',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            }],
            series: [{
                name: '百度',
                type: 'bar',
                barGap: '30%',
                barWidth: 30,
                itemStyle: {
                    color: '#f12711',
                    barBorderRadius: 0,
                },
                stack: '搜索引擎',
                data: [620, 732, 701, 734, 1090, 1130, 1120]
            },
                {
                    name: '谷歌',
                    type: 'bar',
                    barGap: '30%',
                    barWidth: 30,
                    itemStyle: {
                        color: '#f5af19',
                        barBorderRadius: 0,
                    },
                    stack: '搜索引擎',
                    data: [120, 132, 101, 134, 290, 230, 220]
                },]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 8, 27, 28, 30,]
    }, {
        class: 'left11',
        option: {
            title: {
                show: false,// 是否显示
                text: '水平柱状堆叠图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['百度', '谷歌'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'axis',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'shadow',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            xAxis: [{
                show: true,//是否显示X轴
                type: 'value',
                axisLine: {
                    show: true,//是否显示X轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '数值',//X轴名称
                max: 'dataMax',//X轴最大值
                min: 'dataMin',//X轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            }],
            dataZoom: [{//数据区域缩放
                show: false,
            }],
            yAxis: [{
                show: true,//是否显示Y轴
                type: 'category',
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                axisLine: {
                    show: true,//是否显示Y轴线
                    lineStyle: {
                        color: '#eee',//轴线颜色
                        width: 1, //这里是为了突出显示加上的
                    },
                },
                name: '星期',//Y轴名称
                max: 'dataMax',//Y轴最大值
                min: 'dataMin',//Y轴最小值
                inverse: false,//是否反向坐标轴
                axisLabel: {
                    fontSize: 14,//字体大小
                    rotate: 0,//坐标轴旋转角度
                    color: '#eee',//标签颜色
                    interval: 0,//坐标轴分隔线的显示间隔
                },
                splitLine: {
                    show: false,//是否显示分割线

                },
                axisTick: {
                    show: false,//是否显示刻度
                }
            }],
            backgroundColor: 'transparent',//背景颜色
            series: [{
                name: '百度',
                type: 'bar',
                barGap: '30%',
                barWidth: 30,
                itemStyle: {
                    color: '#f12711',
                    barBorderRadius: 0,
                },
                stack: '搜索引擎',
                data: [620, 732, 701, 734, 1090, 1130, 1120]
            },
                {
                    name: '谷歌',
                    type: 'bar',
                    barGap: '30%',
                    barWidth: 30,
                    itemStyle: {
                        color: '#f5af19',
                        barBorderRadius: 0,
                    },
                    stack: '搜索引擎',
                    data: [120, 132, 101, 134, 290, 230, 220]
                },]
        },
        showMenu: [1, 2, 3, 4, 5, 6, 7, 8, 27, 28, 30,]
    }, {
        class: 'left12',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准饼图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false
            },
            series: [{
                type: 'pie',
                name: '今日分布',
                radius: '25%',
                center: ['50%', '50%'],
                label: {
                    show: true,//是否显示标签
                    position: 'outside',//标签位置
                },
                labelLine: {
                    show: true,//标签引导线
                },
                emphasis: {
                    label: {
                        show: true,//是否高亮显示
                        fontSize: 12,//高亮时文字大小
                    },

                },
                data: [{
                    value: 335,
                    name: "Apple",
                    itemStyle: {
                        color:'#f00'
                    }
                }, {
                    value: 310,
                    name: "Grapes",
                    itemStyle: {
                        color: '#00f'
                    }
                }, {
                    value: 234,
                    name: "Pineapples",
                    itemStyle: {
                        color: '#f000f0'
                    }
                }, {
                    value: 135,
                    name: "Oranges",
                    itemStyle: {
                        color: '#0f0'
                    }
                }, {
                    value: 1548,
                    name: "Bananas",
                    itemStyle: {
                        color: '#f0a00f'
                    }
                }]
            },]
        },
        showMenu: [1, 2, 3, 7, 15, 16, 17, 27, 30,]
    }, {
        class: 'left13',
        option: {
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            title: {
                show: false,// 是否显示
                text: '环形饼图',
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
                // 圆弧位置
                center: ['50%', '50%'],
                textStyle: {
                    color: '#ccc',
                    fontSize: 16,
                    fontWeight: 'normal'
                },
            },
            legend: {//图例
                show: false,//是否显示
                data: ['水果'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                containLabel: false
            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
                formatter: '{a} <br/>{b}: {c} ({d}%)'
            },
            backgroundColor: 'transparent',//背景颜色
            series: [
                {
                    name: '水果',
                    type: 'pie',
                    center: ['50%', '50%'],
                    label: {
                        show: true,//是否显示标签
                        position: 'outside',//标签位置
                    },
                    labelLine: {
                        show: true,//标签引导线
                    },
                    radius: ['65%', '80%'],
                    avoidLabelOverlap: false,
                    hoverOffset: 5,
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '30',
                        }
                    },
                    data: [{
                        value: 335,
                        name: "Apple",
                        itemStyle: {
                            color: '#f00'
                        }
                    }, {
                        value: 310,
                        name: "Grapes",
                        itemStyle: {
                            color: '#00f'
                        }
                    }, {
                        value: 234,
                        name: "Pineapples",
                        itemStyle: {
                            color: '#f000f0'
                        }
                    }, {
                        value: 135,
                        name: "Oranges",
                        itemStyle: {
                            color: '#0f0'
                        }
                    }, {
                        value: 1548,
                        name: "Bananas",
                        itemStyle: {
                            color: '#f0a00f'
                        }
                    }]
                }
            ]
        },
        showMenu: [1, 2, 3, 7, 15, 16, 17, 27, 30,]
    }, {
        class: 'left14',
        option: {
            title: {
                show: false,// 是否显示
                text: '单指百分比饼图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                data: ['xxx站点超标率'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                formatter: "{a} <br/>{b}: {c} ({d}%)",
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            backgroundColor: 'transparent',//背景颜色
            color: ['#f12711', '#ccc'],
            series: [{
                name: 'XXX站点超标率',
                type: 'pie',
                labelLine: {
                    show: true,//标签引导线
                },
                emphasis: {
                    label: {
                        show: true,//是否高亮显示
                        fontSize: 12,//高亮时文字大小
                    },

                },
                center: ['50%', '50%'], // 饼图的圆心坐标
                radius: ['70%', '80%'],
                avoidLabelOverlap: false,
                hoverAnimation: false,
                label: { //  饼图图形上的文本标签
                    show: true,
                    position: 'center',
                    color: '#f12711',
                    fontSize: 18,
                    fontWeight: 'bold',
                },
                data: [{
                    value: 80,
                    name: "超标",
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    itemStyle: {
                        color: '#eee'
                    }
                },
                    {
                        value: 60,
                        name: '正常',
                        label: {
                            normal: {
                                show: true
                            }
                        },
                        itemStyle: {
                            color: '#f00'
                        }
                    }]
            }]
        },
        showMenu: [1, 2, 3, 7, 15, 16, 17, 27, 30,]
    }, {
        class: 'left15',
        option: {
            title: {
                show: false,// 是否显示
                text: '南丁格尔玫瑰图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                name: ['面积模式'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                formatter: "{a} <br/>{b}: {c} ({d}%)",
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            backgroundColor: 'transparent',//背景颜色
            series: [{
                name: '面积模式',
                type: 'pie',
                label: {
                    show: true,//是否显示标签
                    position: 'outside',//标签位置
                },
                labelLine: {
                    show: true,//标签引导线
                },
                emphasis: {
                    label: {
                        show: true,//是否高亮显示
                        fontSize: 12,//高亮时文字大小
                    },

                },
                radius: [30, 100],
                center: ['50%', '60%'],
                roseType: 'area',
                data: [{
                    value: 335,
                    name: "Apple",
                    itemStyle: {
                        color: '#f00'
                    }
                }, {
                    value: 310,
                    name: "Grapes",
                    itemStyle: {
                        color: '#00f'
                    }
                }, {
                    value: 234,
                    name: "Pineapples",
                    itemStyle: {
                        color: '#f000f0'
                    }
                }, {
                    value: 135,
                    name: "Oranges",
                    itemStyle: {
                        color: '#0f0'
                    }
                }, {
                    value: 1548,
                    name: "Bananas",
                    itemStyle: {
                        color: '#f0a00f'
                    }
                }]
            }]
        },
        showMenu: [1, 2, 3, 7, 15, 16, 17, 27, 30,]
    }, {
        class: 'left16',
        option: {
            title: {
                show: false,// 是否显示
                text: '活动占比图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                name: ['访问来源'],
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                formatter: '{a} <br/>{b}: {c} ({d}%)',
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#ccc',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '40px',
                top: '20px',
                bottom: '20px',
                right: '20px',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            series: [{
                name: '访问来源',
                type: 'pie',
                radius: ['30%', '25%'],
                center: ['50%', '50%'],
                emphasis: {
                    label: {
                        show: true,//是否高亮显示
                        fontSize: 12,//高亮时文字大小
                    },

                },
                label: {
                    show: false,
                    position: 'inner',
                    color: 'red',
                    fontSize: 18,
                    fontWeight: 'bold',

                },
                avoidLabelOverlap: false,
                hoverAnimation: false,
                labelLine: {
                    show: false
                },
                data: [{
                    value: 90,
                    name: "谷歌",
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    itemStyle:{
                        color:'#f12711'
                    }

                }, {
                    value: 30,
                    name: "百度",
                    label: {
                        normal: {
                            show: false
                        }
                    },
                    itemStyle: {
                        color: '#f5af19'
                    }
                }]
            },
                {
                    name: '访问来源',
                    type: 'pie',
                    radius: ['40%', '55%'],
                    center: ['50%', '50%'],
                    label: {
                        show: false,//是否显示标签
                        position: 'outside',//标签位置
                    },
                    labelLine: {
                        show: true,//标签引导线
                    },
                    emphasis: {
                        label: {
                            show: false,//是否高亮显示
                            fontSize: 12,//高亮时文字大小
                        },

                    },
                    avoidLabelOverlap: false,
                    hoverAnimation: false,
                    data: [{
                        value: 50,
                        name: "谷歌",
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        itemStyle: {
                            color: '#f12711'
                        }
                    }, {
                        value: 80,
                        name: "百度",
                        label: {
                            normal: {
                                show: false
                            }
                        },
                        itemStyle: {
                            color: '#f5af19'
                        }
                    }]
                }]
        },
        showMenu: [1, 2, 3, 7, 15, 16, 17, 27, 30,]
    }, {
        class: 'left17',
        option: {
            title: {
                show: false,// 是否显示
                text: '水球占比图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            series: [{
                type: 'liquidFill',
                radius: '80%',
                data: [0.5, {
                    value: 0.4,
                    direction: 'left',
                    itemStyle: {
                        normal: {
                            color: 'red'
                        }
                    }
                }, 0.3, 0.2],
                label: {
                    normal: {
                        textStyle: {
                            color: 'red',
                            insideColor: 'yellow',
                            fontSize: 50
                        }
                    }
                }
            }]
        },
        showMenu: [1, 2, 3, 7, 16, 17, 27, 30,]
    }, {
        class: 'left18',
        isfun: true,
        option: async function () {
            let data = await $.get('../../lib/json/float.json',)
            return {
                title: {
                    show: false,// 是否显示
                    text: '垂直树图',// 标题内容
                    link: '#',// 文本超链接
                    target: 'blank',// 打开方式
                    textStyle: {
                        color: '#fff',//标题颜色
                        fontSize: 14,//标题字体大小

                    },
                    left: 'left',//标题水平对齐方式
                    top: 'top',//标题垂直对齐方式
                },
                textStyle: {
                    fontSize: 20,
                    fontFamily: '微软雅黑',
                    fontWeight: 'normal',
                    color: '#eee',

                },
                tooltip: {//提示框
                    trigger: 'item',//触发类型
                    textStyle: {
                        color: '#eee',//提示框文本颜色
                        fontSize: 14,//字体大小
                    },
                    triggerOn: 'mousemove',
                    axisPointer: {//指示器
                        type: 'line',//指示器类型
                        lineStyle: {
                            color: '#eee',//线条颜色
                            width: 1,//线条宽度
                        },
                    },
                },
                backgroundColor: 'transparent',//背景颜色
                grid: {
                    left: '5%',
                    top: '5%',
                    bottom: '5%',
                    right: '5%',
                    width: 'auto',
                    height: 'auto',
                    containLabel: false//是否包含标签
                },
                series: [{
                    type: 'tree',
                    data: [data],
                    symbol: 'emptyCircle',
                    orient: 'vertical',
                    expandAndCollapse: true,
                    label: {
                        show: true,//普通标签是否显示
                        position: 'top',
                        rotate: -90,
                        verticalAlign: 'middle',
                        align: 'right',
                        fontSize: 9,//标签字体大小
                        color: '#f12711',//标签颜色
                    },
                    emphasis: {
                        label: {
                            show: true,
                            color: '#f12711',
                            fontSize: 9,
                        }
                    },
                    leaves: {
                        label: {
                            position: 'bottom',
                            rotate: -90,
                            verticalAlign: 'middle',
                            align: 'left'
                        }
                    },
                }]
            }
        },
        showMenu: [1, 2, 7, 18, 27, 28, 30,]
    }, {
        class: 'left19',
        isfun: true,
        option: async function () {
            let data = await $.get('../../lib/json/float.json',)
            echarts.util.each(data.children, function (datum, index) {
                index % 2 === 0 && (datum.collapsed = true);
            })
            return {
                title: {
                    show: false,// 是否显示
                    text: '水平树图',// 标题内容
                    link: '#',// 文本超链接
                    target: 'blank',// 打开方式
                    textStyle: {
                        color: '#fff',//标题颜色
                        fontSize: 14,//标题字体大小

                    },
                    left: 'left',//标题水平对齐方式
                    top: 'top',//标题垂直对齐方式
                },
                textStyle: {
                    fontSize: 20,
                    fontFamily: '微软雅黑',
                    fontWeight: 'normal',
                    color: '#eee',

                },
                tooltip: {//提示框
                    trigger: 'item',//触发类型
                    triggerOn: 'mousemove',
                    textStyle: {
                        color: '#eee',//提示框文本颜色
                        fontSize: 14,//字体大小
                    },
                    axisPointer: {//指示器
                        type: 'line',//指示器类型
                        lineStyle: {
                            color: '#eee',//线条颜色
                            width: 1,//线条宽度
                        },
                    },
                },
                backgroundColor: 'transparent',//背景颜色
                grid: {
                    left: '5%',
                    top: '5%',
                    bottom: '5%',
                    right: '5%',
                    width: 'auto',
                    height: 'auto',
                    containLabel: false//是否包含标签
                },
                series: [{
                    type: 'tree',
                    data: [data],
                    symbolSize: 7,
                    emphasis: {
                        label: {
                            show: true,
                            color: '#eee',
                            fontSize: 9,
                        }
                    },
                    label: {
                        show: true,//普通标签是否显示
                        position: 'left',
                        verticalAlign: 'middle',
                        align: 'right',
                        fontSize: 9,
                        color: '#f12711',//标签颜色
                    },
                    leaves: {
                        label: {
                            position: 'right',
                            verticalAlign: 'middle',
                            align: 'left'
                        }
                    },
                }]
            }
        },
        showMenu: [1, 2, 7, 18, 27, 28, 30,]
    }, {
        class: 'left20',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准雷达图',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小
            
                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            legend: {//图例
                show: false,//是否显示
                left: 'left',//图例水平位置
                top: 'top',//图例垂直位置
                orient: 'horizontal',//列表布局方向
                itemWidth: 25,//图例标记宽度
                itemHeight: 14,//图例标记高度
                textStyle: {
                    color: '#EEE',//图例文字颜色
                    fontSize: 14,//图例文字大小
                }
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',
        
            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            radar: {
                // shape: 'circle',
                name: {
                    show: false
                },
                center: ['50%', '50%'],//中心点设置
                radius: 140,//外半径
                indicator: [
                    {
                        name: '销售（sales）',
                        max: 6500
                    },
                    {
                        name: '管理（Administration）',
                        max: 16000
                    },
                    {
                        name: '信息技术（Information Techology）',
                        max: 30000
                    },
                    {
                        name: '客服（Customer Support）',
                        max: 38000
                    },
                    {
                        name: '研发（Development）',
                        max: 52000
                    },
                    {
                        name: '市场（Marketing）',
                        max: 25000
                    }
                ]
            },
            series: [{
                name: '预算 vs 开销（Budget vs spending）',
                type: 'radar',
                // areaStyle: {normal: {}},
        
                data: [
                    {
                        value: [4300, 10000, 28000, 35000, 50000, 19000],
                        name: '预算分配',
                        lineStyle: {
                            color: '#f12711'
                        },
                    },
                    {
                        value: [5000, 14000, 28000, 31000, 42000, 21000],
                        name: '实际开销',
                        lineStyle: {
                            color: '#f5af19'
                        },
                    }
                ]
            }]
        },
        showMenu: [1, 2, 3, 7, 13, 15, 27, 30,]
    }, {
        class: 'left21',
        option: {
            title: {
                show: false,// 是否显示
                text: '标准仪表盘',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                formatter: '{a} <br/>{b} : {c}%',
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            // radius: ["30%",'50%'],
            series: [
                {
                    name: '业务指标',
                    title: {
                        color: '#f5af19',//标题颜色
                        fontSize: 20,//标题大小
                    },
                    type: 'gauge',
                    radius: '50%',
                    detail: {
                        color: '#f12711',
                        fontSize: 50,//大小
                    },
                    startAngle: 225,//起始角度
                    endAngle: -45,//结束角度
                    min: 0,//最小值
                    max: 100,//最大值
                    data: [{
                        value: 50,
                        name: '完成率',

                    }],
                    axisLabel: {
                        show: true,//刻度是否显示
                        color: '#f12711',//标签颜色1
                        fontSize: 9,
                        formatter: function (e) {
                            switch (e + "") {
                                case "10":
                                    return "0.2";
                                case "20":
                                    return "0.4";
                                case "30":
                                    return "0.6";
                                case "40":
                                    return "0.8";
                                case "50":
                                    return "1";
                                case "60":
                                    return "0.8";
                                case "70":
                                    return "0.6";
                                case "80":
                                    return "0.4";
                                case "90":
                                    return "0.2";
                                case "100":
                                    return "0";
                                default:
                                    return "";
                            }
                        }

                    }
                }
            ]
        },
        showMenu: [1, 2, 7, 19, 27, 30,]
    }, {
        class: 'left22',
        option: {
            title: {
                show: false,// 是否显示
                text: '正负仪表盘',// 标题内容
                link: '#',// 文本超链接
                target: 'blank',// 打开方式
                textStyle: {
                    color: '#fff',//标题颜色
                    fontSize: 14,//标题字体大小

                },
                left: 'left',//标题水平对齐方式
                top: 'top',//标题垂直对齐方式
            },
            textStyle: {
                fontSize: 20,
                fontFamily: '微软雅黑',
                fontWeight: 'normal',
                color: '#eee',

            },
            tooltip: {//提示框
                trigger: 'item',//触发类型
                formatter: '{a} <br/>{b} : {c}%',
                textStyle: {
                    color: '#eee',//提示框文本颜色
                    fontSize: 14,//字体大小
                },
                axisPointer: {//指示器
                    type: 'line',//指示器类型
                    lineStyle: {
                        color: '#eee',//线条颜色
                        width: 1,//线条宽度
                    },
                },
            },
            backgroundColor: 'transparent',//背景颜色
            grid: {
                left: '5%',
                top: '5%',
                bottom: '5%',
                right: '5%',
                width: 'auto',
                height: 'auto',
                containLabel: false//是否包含标签
            },
            series: [
                {
                    name: '业务指标',
                    type: 'gauge',
                    title: {
                        color: '#f5af19',//标题颜色
                        fontSize: 30,//标题大小
                    },
                    radius: '50%',
                    detail: {
                        color: '#f12711',
                        fontSize: 50,//大小
                        formatter: '{value}%',//详情格式器
                    },
                    data: [{
                        value: 50,
                        name: '完成率',

                    }],
                    axisLabel: {
                        color: '#f12711',
                        fontSize: 14,
                        formatter: function (e) {
                            switch (e + "") {
                                case "10":
                                    return "-20";
                                case "20":
                                    return "-15";
                                case "30":
                                    return "-10";
                                case "40":
                                    return "-5";
                                case "50":
                                    return "0";
                                case "60":
                                    return "5";
                                case "70":
                                    return "10";
                                case "80":
                                    return "15";
                                case "90":
                                    return "20";
                                default:
                                    return "";
                            }
                        }

                    }
                }
            ]
        },
        showMenu: [1, 2, 7, 19, 27, 30,]
    }, {
        class: 'left23',
        isfun: true,
        option: async function () {
            let data = await $.get('../../lib/json/100000_full.json',)
            echarts.registerMap('china', data)
            return {
                title: {
                    show: false,// 是否显示
                    text: '中国地图',// 标题内容
                    link: '#',// 文本超链接
                    target: 'blank',// 打开方式
                    textStyle: {
                        color: '#fff',//标题颜色
                        fontSize: 14,//标题字体大小

                    },
                    left: 'left',//标题水平对齐方式
                    top: 'top',//标题垂直对齐方式
                },
                grid: {
                    left: '5%',
                    top: '5%',
                    bottom: '5%',
                    right: '5%',
                    width: '5%',
                    height: 'auto',
                    containLabel: false//是否包含标签
                },
                series: [
                    {
                        type: 'map',
                        map: 'china',
                        roam: false,//是否开启鼠标缩放和平移漫游。
                        zoom: 1,//缩放比例
                        boundingCoords: '115.97,29.71',//定义定位的左上角以及右下角分别所对应的经纬度
                        silent: false,//是否不响应和触发鼠标事件
                        label: {
                            show: false,//是否显示标签
                            color: '#fff',//标签颜色
                            fontSize: 14,//标签大小
                        },
                        emphasis: {
                            label: {
                                show: false,
                                fontSize: 14,
                                color: '#eee',

                            },
                            itemStyle: {
                                areaColor: '#DC2424',//地图区域颜色
                                borderColor: '#000',//图形边框颜色
                                borderWidth: 1,//图形宽度
                            }
                        },
                        itemStyle: {
                            areaColor: '#20BDFF',//地图区域颜色
                            borderColor: '#A5FECB',//图形边框颜色
                            borderWidth: 1,//图形宽度
                            shadowColor: '#000',//阴影颜色
                            shadowBlur: 0,//阴影宽度
                            shadowOffsetX: 0,//阴影垂直方向上的偏移距离。
                            shadowOffsetY: 0,//阴影垂直方向上的偏移距离。
                        },
                    }
                ]
            }
        },
        showMenu: [2, 20, 21, 28, 30,]
    }, {
        class: 'left24',
        isfun: true,
        option: async function () {
            let data = await $.get('../../lib/json/330000_full.json',)
            echarts.registerMap('suzhou', data)
            return {
                title: {
                    show: false,// 是否显示
                    text: '苏州地图',// 标题内容
                    link: '#',// 文本超链接
                    target: 'blank',// 打开方式
                    textStyle: {
                        color: '#fff',//标题颜色
                        fontSize: 14,//标题字体大小

                    },
                    left: 'left',//标题水平对齐方式
                    top: 'top',//标题垂直对齐方式
                },
                grid: {
                    left: '5%',
                    top: '5%',
                    bottom: '5%',
                    right: '5%',
                    width: 'auto',
                    height: 'auto',
                    containLabel: false//是否包含标签
                },
                series: [
                    {
                        type: 'map',
                        map: 'suzhou',
                        roam: false,//是否开启鼠标缩放和平移漫游。
                        zoom: 1,//缩放比例
                        boundingCoords: '115.97,29.71',//定义定位的左上角以及右下角分别所对应的经纬度
                        silent: false,//是否不响应和触发鼠标事件
                        label: {
                            show: false,//是否显示标签
                            color: '#fff',//标签颜色
                            fontSize: 14,//标签大小
                        },
                        emphasis: {
                            label: {
                                show: false,
                                fontSize: 14,
                                color: '#eee',

                            },
                            itemStyle: {
                                areaColor: '#DC2424',//地图区域颜色
                                borderColor: '#000',//图形边框颜色
                                borderWidth: 1,//图形宽度
                            }
                        },
                        itemStyle: {
                            areaColor: '#20BDFF',//地图区域颜色
                            borderColor: '#A5FECB',//图形边框颜色
                            borderWidth: 1,//图形宽度
                            shadowColor: '#000',//阴影颜色
                            shadowBlur: 0,//阴影宽度
                            shadowOffsetX: 0,//阴影垂直方向上的偏移距离。
                            shadowOffsetY: 0,//阴影垂直方向上的偏移距离。
                        },
                    }
                ]
            }
        },
        showMenu: [2, 20, 21, 28, 30,]
    }, {
        class: 'left25',
        isfun: true,
        option: async function () {
            let data = await $.get('../../lib/json/full.json',)
            echarts.registerMap('HZ', data);
            return {
                title: {
                    show: false,// 是否显示
                    text: '杭州市地图',// 标题内容
                    link: '#',// 文本超链接
                    target: 'blank',// 打开方式
                    textStyle: {
                        color: '#fff',//标题颜色
                        fontSize: 14,//标题字体大小

                    },
                    left: 'left',//标题水平对齐方式
                    top: 'top',//标题垂直对齐方式
                },
                grid: {
                    left: '5%',
                    top: '5%',
                    bottom: '5%',
                    right: '5%',
                    width: 'auto',
                    height: 'auto',
                    containLabel: false//是否包含标签
                },
                backgroundColor: 'transparent',
                series: [
                    {
                        type: 'map',
                        map: 'HZ',
                        roam: false,//是否开启鼠标缩放和平移漫游。
                        zoom: 1,//缩放比例
                        boundingCoords: '115.97,29.71',//定义定位的左上角以及右下角分别所对应的经纬度
                        silent: false,//是否不响应和触发鼠标事件
                        label: {
                            show: false,//是否显示标签
                            color: '#fff',//标签颜色
                            fontSize: 14,//标签大小
                        },
                        emphasis: {
                            label: {
                                show: false,
                                fontSize: 14,
                                color: '#eee',

                            },
                            itemStyle: {
                                areaColor: '#DC2424',//地图区域颜色
                                borderColor: '#000',//图形边框颜色
                                borderWidth: 1,//图形宽度
                            }
                        },
                        itemStyle: {
                            areaColor: '#20BDFF',//地图区域颜色
                            borderColor: '#A5FECB',//图形边框颜色
                            borderWidth: 1,//图形宽度
                            shadowColor: '#000',//阴影颜色
                            shadowBlur: 0,//阴影宽度
                            shadowOffsetX: 0,//阴影垂直方向上的偏移距离。
                            shadowOffsetY: 0,//阴影垂直方向上的偏移距离。
                        },
                    }
                ],
            }
        },
        showMenu: [2, 20, 21, 28, 30,]
    },
]
