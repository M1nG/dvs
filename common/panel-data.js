var panelData = {}

panelData.mapData = {}

panelData.junks = {
    file: [],
    panel: [],
    chart: []
}


/*panelData.defaultOption = {
    panel: {
        width: "1920",
        height: "1080",
        coverSetting: "0", //0自动截图  1手动上传
        backgroundType: "1", //0 背景图 1背景色
        backgroundSrc: "#0d2b5e",
        zoomType: "2" //0等比缩放宽度铺满 1等比缩放高度铺满 2全屏铺满 3原始像素
    },
    offset: {left: 0.35, top: 0.35, width: 0.3, height: 0.25}
}*/

panelData._getDefaultOption = function (key) {
    var res = {};
    switch (key) {
        case "panel":
            res = {
                width: "1920",
                height: "1080",
                coverSetting: "0",
                backgroundType: "1",
                backgroundSrc: "#0d2b5e",
                zoomType: "2"
            }
            break;
        case "offset":
            res = {
                left: 0.35,
                top: 0.35,
                width: 0.3,
                height: 0.25
            }
            break;
        case "dataSrc":
            res = {
                "check": "default",
                "default": {}
            }
            break;
        default:
            res = {error: "_getDefaultOption 参数非法", text: arguments[0] + "is not key"};
            break;
    }
    return res;
}

panelData.elements = {
    /*"{elementid}": {
        option: {},
        data:"",
        showMenu: []
    }*/
}

panelData.panels = {
    //结构示例
    /* "{panelid}": {
        name: "",
        showSrc:"",
        option: {
            width: "",
            height: "",
            coverSetting: "",
            backgroundType: "",
            backgroundSrc: "",
            zoomType: ""
        },
        charts: {
            "{modualid}": {
                elementId: "",
                echartIns:null,
                option: "",
                offset: {
                    left: 0,
                    top: 0,
                    width: 0.25,
                    height: 0.3
                },
                dataSrc:""
            }
        }
    } */
}

panelData._genPanel = function (id, name, showSrc, option) {
    panelData.panels[id] = {
        name: name,
        showSrc: showSrc,
        option: !option ? panelData._getDefaultOption("panel") : option,
        charts: {}
    };
}

panelData._getPanel = function (id) {
    return panelData.panels[id];
}

panelData._setPanel = function (id, panel) {
    panelData.panels[id] = panel;
}

panelData._genChart = function (panelId, chartId, elementId, echartIns, option, offset, dataSrc) {
    var defDataSrc = panelData._getDefaultOption("dataSrc");
    defDataSrc.default = panelData._getElement(elementId).data;
    panelData.panels[panelId].charts[chartId] = {
        elementId: elementId,
        echartIns: echartIns,
        option: !option ? panelData._getElement(elementId).option : option,
        offset: !offset ? panelData._getDefaultOption("offset") : offset,
        dataSrc: !dataSrc ? defDataSrc : dataSrc,
    };
}

panelData._getChart = function (panelId, chartId) {
    return panelData.panels[panelId].charts[chartId];
}

panelData._setChart = function (panelId, chartId, chart) {
    panelData.panels[panelId].charts[chartId] = chart;
}

panelData._getCharts = function (panelId) {
    return panelData.panels[panelId].charts;
}

panelData._setChart = function (panelId, charts) {
    panelData.panels[panelId].charts = charts;
}

panelData._genElements = function (elementId, config, showMenu, data) {
    panelData.elements[elementId] = {
        option: config,
        showMenu: showMenu,
        data: data
    };
}

panelData._getElement = function (elementId) {
    return cloneObj(panelData.elements[elementId]);
}

panelData._setElement = function (elementId, config) {
    panelData.elements[elementId] = config;
}

panelData._genMapData = function (code, json) {
    panelData.mapData[code] = json;
}

panelData._toSimpleJSON = function () {
    var simpleJSON = {
        panels: {},
        junks: panelData.junks
    };
    var panels = panelData.panels;
    for (var p in panels) {
        var newCharts = {};
        var charts = panels[p].charts;
        for (var c in charts) {
            var nc = {
                elementId: charts[c].elementId,
                option: charts[c].option,
                offset: charts[c].offset,
                dataSrc:charts[c].dataSrc
            }
            newCharts[c] = nc;
        }
        simpleJSON.panels[p] = {
            option: panels[p].option,
            name: panels[p].name,
            showSrc: panels[p].showSrc,
            charts: newCharts
        }
    }
    return simpleJSON;
}