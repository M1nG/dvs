<div class="layui-colla-item menu-item" show="1" option-name='textStyle'>
                            <h2 class="layui-colla-title">
                                全局文本配置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md6">
                                            <input type="text"
                                                   value="14"
                                                   class="textSize" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='fontSize'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            <i class="iconfont">&#xe616;</i><span>字体</span>
                                        </div>
                                        <div class="layui-col-md8">
                                            <select class="textFamily" option-name='fontFamily'>
                                                <option value="微软雅黑">
                                                    微软雅黑
                                                </option>
                                                <option value="宋体">
                                                    宋体
                                                </option>
                                                <option value="隶书">
                                                    隶书
                                                </option>
                                                <option value="黑体">
                                                    黑体
                                                </option>
                                                <option value="幼圆">
                                                    幼圆
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            字体粗细
                                        </div>
                                        <div class="layui-col-md8">
                                            <select class="textBold" option-name='fontWeight'>
                                                <option value="normal">
                                                    常规
                                                </option>
                                                <option value="bold">
                                                    粗
                                                </option>
                                                <option value="bolder">
                                                    特粗
                                                </option>
                                                <option value="lighter">
                                                    细
                                                </option>
                                                <option value="100">
                                                    100
                                                </option>
                                                <option value="200">
                                                    200
                                                </option>
                                                <option value="300">
                                                    300
                                                </option>
                                                <option value="400">
                                                    400
                                                </option>
                                                <option value="500">
                                                    500
                                                </option>
                                                <option value="600">
                                                    600
                                                </option>
                                                <option value="700">
                                                    700
                                                </option>
                                                <option value="800">
                                                    800
                                                </option>
                                                <option value="900">
                                                    900
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            字体颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor"
                                                 id="textColor" option-name='color'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="2"  option-name='title'>
                            <h2 class="layui-colla-title">
                                标题</h2>
                            <div class="layui-colla-content rigc-title">
                                <div class="layui-fluid Title">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示标题
                                        </div>
                                        <div class="layui-col-md5">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch"
                                                       option-name="show">
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            标题内容
                                        </div>
                                        <div class="layui-col-md8">
                                            <input type="text"
                                                   value=""
                                                   class="inputText"
                                                   option-name='text'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            水平位置
                                        </div>
                                        <div class="layui-col-md8">
                                            <select class="inputAlign" option-name='left'>
                                                <option value="center">
                                                    居中
                                                </option>
                                                <option value="left">
                                                    居左
                                                </option>
                                                <option value="right">
                                                    居右
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            垂直位置
                                        </div>
                                        <div class="layui-col-md8">
                                            <select class="inputVAlign" option-name='top'>
                                                <option value="middle">
                                                    居中
                                                </option>
                                                <option value="top">
                                                    居上
                                                </option>
                                                <option value="bottom">
                                                    居下
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md6">
                                            <input type="text"
                                                   value="14"
                                                   class="inputSize"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='textStyle.fontSize'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            字体颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor"
                                                 id="inputColor" option-name='textStyle.color'></div>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            标题链接
                                        </div>
                                        <div class="layui-col-md8">
                                            <input type="text"
                                                   value=""
                                                   class="inputTarge" option-name='link'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            打开方式
                                        </div>
                                        <div class="layui-col-md8">
                                            <select class="inputOpen" option-name='target'>
                                                <option value="blank">
                                                    在新窗口打开
                                                </option>
                                                <option value="self">
                                                    在当前窗口打开
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="3" option-name='legend'>
                            <h2 class="layui-colla-title">
                                图例</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            <i class="iconfont">&#xe616;</i><span>是否显示</span>
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            图例标记宽度
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="25"
                                                   class="legentWidth"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='itemWidth'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            图例标记高度
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="14"
                                                   class="legentHeight"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='itemHeight'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            列表布局方向
                                        </div>
                                        <div class="layui-col-md7">
                                            <select class="legentOrient" option-name='orient'>
                                                <option value="horizontal">
                                                    水平
                                                </option>
                                                <option value="vertical">
                                                    垂直
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            水平位置
                                        </div>
                                        <div class="layui-col-md7">
                                            <select class="legentLeft" option-name='left'>
                                                <option value="center">
                                                    居中
                                                </option>
                                                <option value="left">
                                                    居左
                                                </option>
                                                <option value="right">
                                                    居右
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            垂直位置
                                        </div>
                                        <div class="layui-col-md7">
                                            <select class="legentTop" option-name='top'>
                                                <option value="middle">
                                                    居中
                                                </option>
                                                <option value="top">
                                                    居上
                                                </option>
                                                <option value="bottom">
                                                    居下
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="14"
                                                   class="legentSize"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='textStyle.fontSize'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor"
                                                 id="legentColor" option-name='textStyle.color'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="4" option-name='dataZoom'>
                            <h2 class="layui-colla-title">
                                数据区域缩放</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md4"
                                             style="line-height:18px">
                                            <i class="iconfont">&#xe616;</i><span>显示数据区域缩放</span>
                                        </div>
                                        <div class="layui-col-md8">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='show'>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="5" option-name='xAxis'>
                            <h2 class="layui-colla-title">
                                x轴</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示x轴
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示轴线
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='axisLine.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            是否反向坐标轴
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='inverse'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示刻度</span></div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='axisTick.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            是否显示分割线
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='splitLine.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            x轴最小值
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="0"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='min'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            x轴最大值
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="100"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='max'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            坐标轴标签间隔
                                        </div>
                                        <div class="layui-col-md7">
                                            <select option-name='axisLabel.interval'>
                                                <option value="0">
                                                    强制显示所有
                                                </option>
                                                <option value="1">
                                                    隔1个标签显示一个标签
                                                </option>
                                                <option value="2">
                                                    隔2个标签显示一个标签
                                                </option>
                                                <option value="3">
                                                    隔3个标签显示一个标签
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            坐标轴标签旋转角度
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="0"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='axisLabel.rotate'>
                                        </div>
                                        <div class="layui-col-md2">
                                            度
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            x轴名称
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="星期" option-name='name'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="14"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='axisLabel.fontSize'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            轴线颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="xAxisLineColor" option-name='axisLine.lineStyle.color'></div>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            标签颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="xAxisLabelColor" option-name='axisLabel.color'></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="6" option-name='yAxis'>
                            <h2 class="layui-colla-title">
                                y轴</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示y轴
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示轴线
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='axisLine.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            是否反向坐标轴
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='inverse'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否显示刻度</span></div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='axisTick.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            是否显示分割线
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='splitLine.show'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            y轴最小值
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="0"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='min'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            y轴最大值
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="100"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='max'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            坐标轴标签间隔
                                        </div>
                                        <div class="layui-col-md7">
                                            <select option-name='axisLabel.interval'>
                                                <option value="0">
                                                    强制显示所有
                                                </option>
                                                <option value="1">
                                                    隔1个标签显示一个标签
                                                </option>
                                                <option value="2">
                                                    隔2个标签显示一个标签
                                                </option>
                                                <option value="3">
                                                    隔3个标签显示一个标签
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            坐标轴标签旋转角度
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="0"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='axisLabel.rotate'>
                                        </div>
                                        <div class="layui-col-md2">
                                            度
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            y轴名称
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="星期" option-name='name'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="12"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='axisLabel.fontSize'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            轴线颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="yAxisLineColor" option-name='axisLine.lineStyle.color'></div>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            标签颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="yAxisLabelColor" option-name='axisLabel.color'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="7" option-name='backgroundColor'>
                            <h2 class="layui-colla-title">
                                背景</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            背景颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="backGroundColor" option-name='backgroundColor'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="8" option-name='series'>
                            <h2 class="layui-colla-title">
                                系列样式配置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-collapse">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="9" option-name='series'>
                            <h2 class="layui-colla-title">
                                数据系列</h2>
                            <div class="layui-colla-content">
                                <div class="layui-collapse">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="13"  option-name='radar'>
                            <h2 class="layui-colla-title">
                                内外半径及中心点设置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            中心点水平位置
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="50"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='center'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            中心点垂直位置
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="50"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='center'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            外半径
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="80"  onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='radius'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="15" option-name='series'>
                            <h2 class="layui-colla-title">数据系列</h2>
                            <div class="layui-colla-content">

                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="16"  option-name='series'>
                            <h2 class="layui-colla-title">
                                数据系列</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="17" option-name='series'>
                            <h2 class="layui-colla-title">
                                标签相关设置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="18" option-name='series'>
                            <h2 class="layui-colla-title">
                                标签设置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-collapse">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="19"  option-name='series'>
                            <h2 class="layui-colla-title">
                                仪表盘设置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">

                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="20" option-name='series'>
                            <h2 class="layui-colla-title">
                                地图全局配置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5" style="line-height: 18px">
                                            开启鼠标缩放平移
                                        </div>
                                        <div class="layui-col-md3">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-filter="MenuSwitch"
                                                       lay-skin="switch" option-name='roam'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5" style="line-height: 18px">
                                            当前视角缩放比例
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="1" onkeyup="value=value.replace(/[^\d]/g,'')"   option-name='zoom'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5" style="line-height: 18px">
                                            <i class="iconfont">&#xe616;</i><span>当前视角中心维度</span>
                                        </div>
                                        <div class="layui-col-md7">
                                            <input type="text"
                                                   value="225" onkeyup="value=value.replace(/[^\d]/g,'')"   option-name='boundingCoords'>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5" style="line-height: 18px">
                                            <i class="iconfont">&#xe616;</i><span>不响应鼠标事件</span>
                                        </div>
                                        <div class="layui-col-md3">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-filter="MenuSwitch"
                                                       lay-skin="switch" option-name='slient'>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-collapse">
                                    <div class="layui-colla-item">
                                        <h2 class="layui-colla-title">
                                            普通状态样式</h2>
                                        <div class="layui-colla-content">
                                            <div class="layui-fluid">
                                                <div class="layui-row">
                                                    <div class="layui-col-md5" style="line-height: 18px">
                                                        普通状态标签显示
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <form action=""
                                                              class="layui-form">
                                                            <input type="checkbox"
                                                                   name="switch"
                                                                   lay-filter="MenuSwitch"
                                                                   lay-skin="switch" option-name='label.show'>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">普通状态地图边框粗细
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text" value="" onkeyup="value=value.replace(/[^\d]/g,'')"   option-name='itemStyle.borderWidth'>
                                                    </div>
                                                    <div class="layui-col-md2">像素</div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5" style="line-height: 18px">
                                                        普通状态地图边框颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor" id="MapBorderColor" option-name='itemStyle.borderColor'></div>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5" style="line-height: 18px">
                                                        普通状态地图区域颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor" id="MapAreaColor" option-name='itemStyle.areaColor'></div>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5">
                                                        阴影颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor"id="MapShadowColor" option-name='itemStyle.shadowColor'></div>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        阴影模糊距离
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text"
                                                               value="0" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='itemStyle.shadowBlur'>
                                                    </div>
                                                    <div class="layui-col-md2">
                                                        像素
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        阴影水平方向偏移
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text"
                                                               value="0" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='itemStyle.shadowOffsetX'>
                                                    </div>
                                                    <div class="layui-col-md2">
                                                        像素
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        阴影垂直方向偏移
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text"
                                                               value="0" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='itemStyle.shadowOffsetY'>
                                                    </div>
                                                    <div class="layui-col-md2">
                                                        像素
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="layui-colla-item">
                                        <h2 class="layui-colla-title">
                                            高亮状态样式</h2>
                                        <div class="layui-colla-content">
                                            <div class="layui-fluid">
                                                <div class="layui-row">
                                                    <div class="layui-col-md5" style="line-height: 18px">
                                                        高亮状态标签显示
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <form action=""
                                                              class="layui-form">
                                                            <input type="checkbox"
                                                                   name="switch"
                                                                   lay-filter="MenuSwitch"
                                                                   lay-skin="switch" option-name='emphasis.label.show'>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                       高亮状态标签大小
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text"
                                                               value="14" onkeyup="value=value.replace(/[^\d]/g,'')"   option-name='emphasis.label.fontSize'>
                                                    </div>
                                                    <div class="layui-col-md2">
                                                        像素
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        高亮状态标签颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor" id="MapEmpLabelColor" option-name='emphasis.label.color'></div>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        高亮状态地图边框粗细
                                                    </div>
                                                    <div class="layui-col-md5">
                                                        <input type="text" onkeyup="value=value.replace(/[^\d]/g,'')"   option-name='emphasis.itemStyle.borderWidth'>
                                                    </div>
                                                    <div class="layui-col-md2">像素</div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5" style="line-height: 18px">
                                                        高亮状态地图边框颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor" id="MapEmpBorderColor" option-name='emphasis.itemStyle.borderColor'></div>
                                                    </div>
                                                </div>
                                                <div class="layui-row">
                                                    <div class="layui-col-md5"
                                                         style="line-height: 18px">
                                                        高亮状态地图区域颜色
                                                    </div>
                                                    <div class="layui-col-md3">
                                                        <div class="mycolor" id="MapEmpAraeColor" option-name='emphasis.itemStyle.areaColor'></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="21">
                            <h2 class="layui-colla-title">
                                省市选择</h2>
                            <div class="layui-colla-content layui-form">
                                <div class="layui-fluid" id="area-picker">

                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            省
                                        </div>
                                        <div class="layui-col-md8">
                                            <select name="province"
                                                    class="province-selector"
                                                    data-value=""
                                                    lay-filter="province-1">
                                                <option value="">
                                                    请选择省
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            市
                                        </div>
                                        <div class="layui-col-md8">
                                            <select name="city"
                                                    class="city-selector"
                                                    data-value=""
                                                    lay-filter="city-1">
                                                <option value="">
                                                    请选择市
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md4">
                                            区
                                        </div>
                                        <div class="layui-col-md8">
                                            <select name="county"
                                                    class="county-selector"
                                                    data-value=""
                                                    lay-filter="county-1">
                                                <option value="">
                                                    请选择区
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="27" option-name='tooltip'>
                            <h2 class="layui-colla-title">
                                提示框组件</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            <i class="iconfont">&#xe616;</i><span>触发类型</span>
                                        </div>
                                        <div class="layui-col-md7">
                                            <select option-name='trigger'>
                                                <option value="axis">
                                                    坐标轴触发
                                                </option>
                                                <option value="item">
                                                    数据项触发
                                                </option>
                                                <option value="none">
                                                    不触发
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体大小
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="14" option-name='textStyle.fontSize' onkeyup="value=value.replace(/[^\d]/g,'')" >
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            字体颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="ToolTipTextColor" option-name='textStyle.color'></div>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            指示器类型
                                        </div>
                                        <div class="layui-col-md7">
                                            <select option-name='axisPointer.type'>
                                                <option value="line">
                                                    直线指示器
                                                </option>
                                                <option value="shadow">
                                                    阴影指示器
                                                </option>
                                                <option value="cross">
                                                    十字准星指示器
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            线条宽度
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="1" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='axisPointer.lineStyle.width'>
                                        </div>
                                        <div class="layui-col-md2">
                                            像素
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            线条颜色
                                        </div>
                                        <div class="layui-col-md3">
                                            <div class="mycolor" id="ToolTipLineColor" option-name='axisPointer.lineStyle.color'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="28" option-name='grid'>
                            <h2 class="layui-colla-title">
                                边距设置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            <i class="iconfont">&#xe616;</i><span>是否包含标签</span>
                                        </div>
                                        <div class="layui-col-md7">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch" option-name='containLabel'>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            距离容器左侧距离
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="10" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='left'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            距离容器右侧距离
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="10" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='right'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                             style="line-height:18px">
                                            距离容器顶部距离
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="10" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='top'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                    <div class="layui-row">
                                        <div class="layui-col-md5"
                                        style="line-height:18px">
                                            距离容器底部距离
                                        </div>
                                        <div class="layui-col-md5">
                                            <input type="text"
                                                   value="10" onkeyup="value=value.replace(/[^\d]/g,'')"  option-name='bottom'>
                                        </div>
                                        <div class="layui-col-md2">
                                            %
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item menu-item" show="30">
                            <h2 class="layui-colla-title">
                                组件动画配置</h2>
                            <div class="layui-colla-content">
                                <div class="layui-fluid">
                                    <div class="layui-row">
                                        <div class="layui-col-md5">
                                            是否开启动画
                                        </div>
                                        <div class="layui-col-md3">
                                            <form action=""
                                                  class="layui-form">
                                                <input type="checkbox"
                                                       name="switch"
                                                       lay-skin="switch"
                                                       lay-filter="MenuSwitch">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>