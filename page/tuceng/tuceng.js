layui.use(['slider', 'laydate', 'form', 'colorpicker', 'element', 'layarea', 'layer'], function () {
    var layer = layui.layer;
    var slider = layui.slider;
    var form = layui.form;
    var colorpicker = layui.colorpicker;
    var element = layui.element;
    var layarea = layui.layarea;
    var laydate = layui.laydate;
    slider.render({
        elem: '#slideTest1', //绑定元素
        value: 30, //初始值
        theme: '#88d5f2', //主题色
    });
    layarea.render({
        elem: "#area-picker",
        data: {
            province: '上海市', //省
            city: '上海市', //市
            county: '宝山区', //区
        },
        change: function (res) {
            console.log(res);
        }
    })
    element.render('collapse');

    // 发布监听提交
    form.on('switch(FB-BTN)', function (data) {
        var checked = data.elem.checked;
        $(data.elem).closest(".layui-input-block").children(":text:last-child").attr("disabled", !checked);
    })

    form.render();

    laydate.render({
        elem: '#fabu-data',
        type: 'date',
        done: function (value, date) {
            $("#fabu-data").attr("data-value", value);
        }
    });
})
// 左右移动
var qietag = 0;
$('.LEFT').click(function () {
    if (qietag == 0) {
        $('.layui-body').css('left', '0');
        $('.layui-footer').css('left', '0');
        $('.LEFT').css('left', '0');
        $('.LEFT-img1').css('display', 'block');
        $('.LEFT-img2').css('display', 'none');
        qietag = 1;
    } else {
        $('.layui-body').css('left', '220px');
        $('.layui-footer').css('left', '220px');
        $('.LEFT').css('left', '200px');
        $('.LEFT-img2').css('display', 'block');
        $('.LEFT-img1').css('display', 'none');
        qietag = 0;
    }
});

$('.RIGHT').click(function () {
    if (qietag == 0) {
        $('.layui-body').css('right', '0');
        $('.layui-footer').css('right', '0');
        $('.RIGHT').css('right', '0');
        $('.RIGHT-img1').css('display', 'block');
        $('.RIGHT-img2').css('display', 'none');
        qietag = 1;
    } else {
        $('.layui-body').css('right', '240px');
        $('.layui-footer').css('right', '240px');
        $('.RIGHT').css('right', '220px');
        $('.RIGHT-img2').css('display', 'block');
        $('.RIGHT-img1').css('display', 'none');
        qietag = 0;
    }
});

// 发布
$('.fabu-open').on('click', function () {
    panelOptionSync();
    $('.fabu').show();
});

// 发布关闭
$('.fabu-close').on('click', function () {
    $('.fabu').hide();
});

// 发布确认
$('.fabu-sucess').on('click', function () {
    var id = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var code = $("#fabu-yzm").val().trim();
    var time = !$("#fabu-data").attr("data-value") ? "-1" : $("#fabu-data").attr("data-value");
    var repeatMinute = parseInt($("#fabu-repeat").val().trim());
    showLoading();
    $.ajax({
        type: 'post',
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(panelData._toSimpleJSON()),
        url: ctp + "/panel/savePanelGroup",
        success: function (result) {
            closeLoading();
            if (result.code == "200") {
                $.ajax({
                    type: "post",
                    url: ctp + "/release/initToken",
                    data: {
                        "id": id,
                        "code": code,
                        "time": time,
                        "repeatMinute": repeatMinute,
                    },
                    success: function (result) {
                        closeLoading();
                        if (result.code == "200") {
                            var url = window.location.href.substring(0, window.location.href.indexOf(window.location.pathname + window.location.search)) + "/page/fabu/fabu.html?code=" + code + "&token=" + result.data;
                            $("#fabu-textarea").val(url);
                            alert("发布成功！ 请在链接处查看您的链接 。");
                        } else {
                            closeLoading();
                            alert(result.msg);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("网络异常");
                        closeLoading();
                    }
                });
            } else {
                closeLoading();
                alert(result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            closeLoading();
            console.alert("网络请求失败，发布失败，请重试!");
        }
    });
});

// 帮助
$('.help').on('click', function () {
    $.ajax({
        type: 'post',
        data: '',
        url: '#',
        success: function (data) {
            console.log(data)
            alert('请选择帮助');
        }
    })
});

// 异常
$('.errop').on('click', function () {

});

// 保存
$('.Save').on('click', function () {
    panelOptionSync();
    showLoading();
    $.ajax({
        type: 'post',
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(panelData._toSimpleJSON()),
        url: ctp + "/panel/savePanelGroup",
        success: function (result) {
            closeLoading();
            if (result.code == "200") {
                alert("保存成功！");
            } else {
                alert(result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            closeLoading();
            console.alert("网络请求失败，保存失败，请重试!");
        }
    });
});

// 预览
$('.yulan-bar').on('click', function () {
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id")
    panelOptionSync();
    window.open("../yulan/yulan.html?panelId=" + panelId);
});


//面板切换
$('.togge').on('click', function () {
    $('#div3').hide();
    $('#div2').toggle();
    $('#div4').toggle();
    $('#div5').toggle();
    if ($(this).attr("show-tag") == 'panel') {
        if ($("#div5 .ChildC-input[child-checked]").length < 1) {
            $("#div5 .Child-check:first input[name='mod']").click();
        }
    } else {
        if ($("#div4 .ChildC-input[child-checked]").length < 1) {
            $("#div4 .Child-check:first input[name='tex']").click();
        }
    }
});

//点击大屏单选按钮
$("#div4").on("click", ":radio[name=tex]", function () {
    var parent = $(this).parent(".ChildC-input");
    var checked = parent.attr("Child-checked");
    var id = $(this).attr("data-id");
    var panel = panelData._getPanel(id);
    var panelName = panel.name;
    if (!checked) {
        $('#panel_name').val(panelName);
        $("#div4 .ChildC-input[Child-checked]").removeAttr("Child-checked");
        parent.attr("Child-checked", "checked");
        panelOptionInit(id);
        // 渲染组件
        var modules = panelData._getCharts(id);
        for (var moduleId in modules) {
            moduleListInit(id, moduleId);
            moduleInit(id, moduleId);
        }
    }
});

//新增大屏
$("#div4").on("click", ".insertPanel", function () {
    var groupId = getUrlParam("groupId");
    $.ajax({
        type: "post",
        url: ctp + "/panel/insertPanel",
        cache: false,
        async: false,
        data: {"groupId": groupId},
        success: function (result) {
            if (result.code == "200") {
                var panel = result.data;
                panelData._genPanel(panel.panelId, panel.panelName, panel.fileUrl, JSON.parse(panel.configJson));
                panelListInit(panel.panelId);
                $("#div4 .Child-check:first input[name='tex']").click();
            } else {
                alert(result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("网络异常");
        }
    });
});

//大屏组件切换
$("#div5").on("click", ":radio[name='mod']", function () {
    var parent = $(this).parent(".ChildC-input");
    var checked = parent.attr("Child-checked");
    if (!checked) {
        var id = $(this).attr("data-id");
        var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
        if ($("#container .dragable#" + id).length < 1) {
            moduleInit(panelId, id);
        }
        $("#div5 .ChildC-input[Child-checked]").removeAttr("Child-checked");
        parent.attr("Child-checked", "checked");
        $("#container .dragable#" + id).click();
        //todo 选中状态z-index的值变化
    }
});

//大屏删除
$("#div4").on("click", ".delPanel", function () {
    var that = $(this);
    var panelId = that.attr("data-id");
    var confim = confirm('你确定要删除吗?');
    //增加判断最后一个大屏 不允许删除
    var number = $("#div4 div[class = 'Child-check']").length;
    if (confim == true) {
        if (number == 1) {
            alert("至少有一个大屏存在！如若想删除请去数据大屏删除");
        } else {
            $.ajax({
                type: "post",
                url: ctp + "/panel/deletePanelAndImage",
                cache: false,
                data: {"panelId": panelId},
                async: false,
                success: function (result) {
                    if (result.code == "200") {
                        delete panelData.panels[panelId];
                        that.closest(".Child-check").remove();
                        if (that.closest(".Child-check").find("ChildC-input").is("[child-checked]")) {
                            $("#container").empty();
                            $("#div5 .Child-check").remove();
                            $("#Menv").children('.menu-item').hide();
                            $(".Menvtilte").show();
                        }
                        $("#div4 .Child-check:first input[name='tex']").click();
                    } else {
                        alert(result.msg);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("网络异常");
                }
            });
        }
    }
});

//组件删除
$("#div5").on("click", ".delModule", function () {
    //判断是否选中要删除的组件
    if ($("#div5 .ChildC-input[Child-checked]").length == 0) {
        alert("请勾选需要删除的组件！");
    } else {
        var cc = $("#div5 .ChildC-input[Child-checked]");
        var moduleId = cc.children("input").attr("data-id");
        var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
        var confim = confirm('你确定要删除吗?');
        if (confim == true) {
            $("#div5 .ChildC-input[Child-checked]").closest(".Child-check").remove();
            delEcharts(panelId, moduleId);
        }
    }
});


/*画布图层的事件监听*/

$("#container").on("click", ".dragable", function () {
    var id = $(this).attr("id");
    var panelId = $(this).attr("panel-id");
    $("#container").find(".dragable:not(#" + id + ")").css("background", "transparent");
    $(this).focus();
    //切换到组件面板
    if ($("#div5").is(":hidden")) {
        $("#div4 .togge").click();
    }
    $(this).css("background-color", "rgba(0,0,0,0.5)");
    var parent = $("#div5 .ChildC-input :radio[data-id='" + id + "']").parent(".ChildC-input");
    if (!parent.is('[Child-checked]')) {
        $("#div5 .ChildC-input[Child-checked]").removeAttr("Child-checked");
        $("#div5 .ChildC-input #module-" + id).click();
        return;
    }
    var elementId = $(this).attr("element-id");
    var menus = panelData._getElement(elementId).showMenu;
    //右侧设置区的渲染
    $("#Menv").attr("panel-id", panelId);
    $("#Menv").attr("element-id", elementId);
    $("#Menv").attr("module-id", id);
    $("#Menv").children('.menu-item').hide();
    $(".Menvtilte").hide();
    for (var m in menus) {
        $("#Menv .menu-item[show='" + menus[m] + "']").show();
    }
    moduleOptionInit(panelId, id);
});

function addModuleEvents() {

    $('#container').off('resize').on('resize', function () {
        $('#container').find(".dragable").each(function () {
            var panelId = $(this).attr("panel-id");
            var id = $(this).attr("id");
            panelData._getChart(panelId, id).echartIns.resize();
            //todo 长宽高的重新计算 fucntion repeatEchart(){}
        });
    });

    $('#container .dragable').off('keyup').on('keyup', function (e) {
        if (e.which == "8" || e.which == "46") {
            var confim = confirm('你确定要删除吗?');
            if (confim == true) {
                var dragable = $(e.currentTarget).closest(".dragable");
                var id = dragable.attr('id');
                var panelId = dragable.attr('panel-id');
                delEcharts(panelId, id);
            }
        }
    });

    $('#container .dragable')
        .resizable({
            containment: '#container',
            handles: "all",
            autoHide: true,
            minHeight: 50,
            minWidth: 100,
            stop: function (event, ui) {
                var that = $(ui.helper[0]);
                var chart = panelData._getChart(that.attr("panel-id"), that.attr("id"));
                chart.echartIns.resize();
                chart.offset.left = calculateFormat(ui.position.left, $("#container").width());
                chart.offset.top = calculateFormat(ui.position.top, $("#container").height());
                chart.offset.width = calculateFormat(ui.size.width, $("#container").width());
                chart.offset.height = calculateFormat(ui.size.height, $("#container").height());
            }
        })
        .draggable({
            containment: '#container',
            stop: function (event, ui) {
                var that = $(ui.helper[0]);
                var chart = panelData._getChart(that.attr("panel-id"), that.attr("id"));
                chart.echartIns.resize();
                chart.offset.left = calculateFormat(ui.position.left, $("#container").width());
                chart.offset.top = calculateFormat(ui.position.top, $("#container").height());
            }
        });
}

//添加大屏组元素
$('.layui-layout-left').on('click', '.element', function () {
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var moduleId = guid();
    panelData._genChart(panelId, moduleId, $(this).attr("data-element-id"));
    moduleListInit(panelId, moduleId);
    moduleInit(panelId, moduleId);
});

/* 大屏设置相关 */

// 背景图/色 切换
$('#div1 input[name="color"]').on("click", function () {

    var colorType = $("input[name='color']:checked").val();
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    if (colorType == 0) {
        $('#container').css('background-color', 'none');
        backgroundSrc = $("#div1  #avarimgs").attr("src")
        $('#container').css('background-image', 'url(' + backgroundSrc + ')');
    } else {
        $('#container').css('background-image', 'none');
        backgroundSrc = $("#container").css("background-color");
        $("#mycolor").attr("data-color", backgroundSrc);
        renderColorpicker('#mycolor', backgroundSrc, function (color) {
            $('#container').css('background-color', color);
            panelOptionSync();
        });
    }
    var zoomType = $("#div1 input[name='pu']:checked").val();
    var width = $("#div1 input[name='p-width']").val();
    var height = $("#div1 input[name='p-height']").val();
    if (zoomType == "0") {
        $('#container').css('background-size', width + "px auto");
    } else if (zoomType == "1") {
        $('#container').css('background-size', "auto " + height + "px");
    } else if (zoomType == "2") {
        $('#container').css('background-size', "100% 100%");
    } else if (zoomType == "3") {
        $('#container').css('background-size', width + "px " + height + "px");
    }
    //sync 同步大屏设置
    panelOptionSync();
});

//背景图上传
function xmTanUploadImg(obj) {
    var imgType = $(obj).attr('upload-type');
    //存到 panel 表里面的参数 存入
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var formData = new FormData();
    formData.append('imgType', imgType);//0为背景，1为封面
    formData.append('panelId', panelId)
    formData.append("file", obj.files[0]);
    if (imgType == 0) {
        //formData.append("fileId", "");

        //是否要将上传未保存的文件及时删除按照架构的演进再看
        //如果需要删除 panelData.junks.file, 后台统一删除
        $.ajax({
            type: "post",
            url: ctp + "/panel/uploadAndSavePanel",
            cache: false,
            data: formData,
            processData: false,
            contentType: false,
            async: false,
            success: function (result) {
                if (result.code == "200") {
                    var url = ctp + "/static/" + result.data.backgroundUrl;
                    fileId = result.data.backgroundId;
                    $(obj).attr("data-id", fileId);
                    $('#container').css('background-image', 'url(' + url + ')');
                    $('#avarimgs').attr("src", url);
                    panelOptionSync();
                } else {
                    alert(result.msg);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("网络异常");
            }
        });

    } else {
        $.ajax({
            type: "post",
            url: ctp + "/panel/uploadAndSavePanel",
            cache: false,
            data: formData,
            processData: false,
            contentType: false,
            async: false,
            success: function (result) {
                if (result.code == "200") {
                    var url = ctp + "/static/" + result.data.fileUrl;
                    $("#div4 .ChildC-input[child-checked]").next("label[for='panel-" + panelId + "']").children("img").attr("src", url);
                    $('#CoverPng').attr("src", url);
                } else {
                    alert(result.msg);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("网络异常");
            }
        });
    }

}

// 缩放类型
$('#div1 input[name="pu"]').on("change", function () {
    var zoomType = $("#div1 input[name='pu']:checked").val();
    var width = $("#div1 input[name='p-width']").val();
    var height = $("#div1 input[name='p-height']").val();
    if (zoomType == "0") {
        $('#container').css('background-size', width + "px auto");
    } else if (zoomType == "1") {
        $('#container').css('background-size', "auto " + height + "px");
    } else if (zoomType == "2") {
        $('#container').css('background-size', "100% 100%");
    } else if (zoomType == "3") {
        $('#container').css('background-size', width + "px " + height + "px");
    }
    //sync 同步大屏设置
    panelOptionSync();
});

// 封面设置
$('#div1 input[name="zhi"]').on("change", function () {
    panelOptionSync();
});

$('#div1').on('change', "input[name='p-width']", function () {
    var zoomType = $("#div1 input[name='pu']:checked").val();
    var width = $("#div1 input[name='p-width']").val();
    var height = $("#div1 input[name='p-height']").val();
    if (zoomType == "0") {
        $('#container').css('background-size', width + "px auto");
    } else if (zoomType == "1") {
        $('#container').css('background-size', "auto " + height + "px");
    } else if (zoomType == "2") {
        $('#container').css('background-size', "100% 100%");
    } else if (zoomType == "3") {
        $('#container').css('background-size', width + "px " + height + "px");
    }
    panelOptionSync();
});

$('#div1').on('change', "input[name='p-height']", function () {
    var zoomType = $("#div1 input[name='pu']:checked").val();
    var width = $("#div1 input[name='p-width']").val();
    var height = $("#div1 input[name='p-height']").val();
    if (zoomType == "0") {
        $('#container').css('background-size', width + "px auto");
    } else if (zoomType == "1") {
        $('#container').css('background-size', "auto " + height + "px");
    } else if (zoomType == "2") {
        $('#container').css('background-size', "100% 100%");
    } else if (zoomType == "3") {
        $('#container').css('background-size', width + "px " + height + "px");
    }
    panelOptionSync();
})

function getCover(id) {
    domtoimage.toPng(document.getElementById('container'))
        .then(function (dataUrl) {
            $("#div4 .Child-check>label[for='panel-" + id + "'] img").attr("src", dataUrl);
            panelData._getPanel(id).showSrc = dataUrl;
        })
        .catch(function (error) {
            console.error('oops, something went wrong!', error);
        });
}

/*组件设置相关*/

// 组件删除
function delEcharts(panelId, moduleId) {
    delete panelData.panels[panelId].charts[moduleId];
    panelData.junks.chart.push(moduleId);
    $("#div5  #module-" + moduleId).closest(".Child-check").remove();
    $("#container #" + moduleId).remove();
    $("#Menv").children('.menu-item').hide();
    $(".Menvtilte").show();
}

var jsoneditor_default, jsoneditor_api, jsoneditor_database;
jsoneditor_default = new JSONEditor(document.getElementById("jsoneditor_default"), {mode: 'form'}, {});
jsoneditor_api = new JSONEditor(document.getElementById("jsoneditor_api"), {mode: 'view'}, {"text": "等待api回传数据中..."});
jsoneditor_database = new JSONEditor(document.getElementById("jsoneditor_database"), {mode: 'view'}, {"text": "等待数据库回传数据中..."});

// 编辑数据源
$("#edit-datasource").on('click', function () {
    var tag = $(".datasource-list button.check").attr("data-tag");
    $(".dataMenuCont .layui-tab-title li.layui-this").removeClass("layui-this");
    $(".dataMenuCont .layui-tab-title li[data-tag='" + tag + "']").addClass("layui-this");
    $(".dataMenuCont .layui-tab-title .layui-tab-item.layui-show").removeClass("layui-show");
    var content = $(".dataMenuCont .layui-tab-content .layui-tab-item[data-tag='" + tag + "']");
    content.addClass("layui-show");
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var moduleId = $("#div5 .ChildC-input[child-checked]>:radio").attr("data-id");
    var chart = panelData._getChart(panelId, moduleId);
    var dataSrc = chart.dataSrc;
    $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='default'] .dataMenu-input :text").val("静态数据");
    $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='api'] .dataMenu-input :text").val(!dataSrc.api ? "" : dataSrc.api);
    $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='database'] .dataMenu-input :text").val(!dataSrc.database ? "" : dataSrc.database);
    postModuleData(chart, "all", true);
    $(".dataMenu").show();
});

//网络获取数据
function postModuleData(chart, type, async) {
    var res = {};
    if (async == null || async == undefined) {
        async = true;
    }
    if (type == "default") {
        if (async) {
            jsoneditor_default.setText(JSON.stringify(chart.dataSrc.default));
        } else {
            res = chart.dataSrc.default;
        }
    } else if (type == "api") {
        if (chart.dataSrc.api) {
            $.ajax({
                type: "post",
                async: async,
                url: chart.dataSrc.api,
                success: function (data) {
                    if (async) {
                        jsoneditor_api.setText(data);
                    } else {
                        res = data;
                    }
                },
                error: function () {
                    if (async) {
                        jsoneditor_api.setText({"text": "请求api失败..."});
                    } else {
                        alert("请求api失败...请检查网络或更换静态数据源再试！")
                    }
                }
            });
        }
    } else if (type == "database") {
        if (chart.dataSrc.database) {
            $.ajax({
                type: "post",
                async: async,
                data: {"database": chart.dataSrc.database},
                url: ctp + "/module/getModuleDatabase",
                success: function (result) {
                    if(result.code == "200"){
                        jsoneditor_database.setText(JSON.stringify(result.data));
                    }else{
                        alert(result.msg);
                    }
                },
                error: function () {
                    if (async) {
                        jsoneditor_database.setText({"text": "请求数据库失败..."});
                    } else {
                        alert("请求数据库失败...请检查网络或更换静态数据源再试！")
                    }
                }
            });
        }
    } else {
        jsoneditor_default.setText(JSON.stringify(chart.dataSrc.default));
        if (chart.dataSrc.api) {
            $.ajax({
                type: "post",
                async: async,
                url: chart.dataSrc.api,
                success: function (data) {
                    jsoneditor_api.setText(data);
                },
                error: function () {
                    jsoneditor_api.setText({"text": "请求api失败..."});
                }
            });
        }
        if (chart.dataSrc.database) {
            $.ajax({
                type: "post",
                async: async,
                data: {"database": chart.dataSrc.database},
                url: ctp + "/module/getModuleDatabase",
                success: function (result) {
                    if(result.code == "200"){
                        jsoneditor_database.setText(JSON.stringify(result.data));
                    }else{
                        alert(result.msg);
                    }
                },
                error: function () {
                    jsoneditor_database.setText({"text": "请求数据库失败..."});
                }
            });
        }
    }
    return res;
}

$(".postModuleData").on("click", function () {
    var tag = $(this).closest(".layui-tab-item").attr("data-tag");
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var moduleId = $("#div5 .ChildC-input[child-checked]>:radio").attr("data-id");
    var chart = panelData._getChart(panelId, moduleId);
    postModuleData(chart, tag, true);
})

$('.data-confirm').on('click', function () {
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var moduleId = $("#div5 .ChildC-input[child-checked]>:radio").attr("data-id");
    var chart = panelData._getChart(panelId, moduleId);
    chart.dataSrc.default = jsoneditor_default.get();
    chart.dataSrc.api = $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='api'] .dataMenu-input :text").val();
    chart.dataSrc.database = $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='database'] .dataMenu-input :text").val();
    $('.dataMenu').hide();
    moduleOptionSync();
})

$('.dataClose').on('click', function () {
    jsoneditor_default = new JSONEditor(document.getElementById("jsoneditor_default"), {mode: 'form'}, {});
    jsoneditor_api = new JSONEditor(document.getElementById("jsoneditor_api"), {mode: 'view'}, {"text": "等待api回传数据中..."});
    jsoneditor_database = new JSONEditor(document.getElementById("jsoneditor_database"), {mode: 'preview'}, {"text": "等待数据库回传数据中..."});
    $('.dataMenu').hide();
})

// 数据源切换
$('.datasource-list').on("click", "button", function () {
    $('.datasource-list button.check').removeClass("check");
    $(this).addClass('check');
    var panelId = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var moduleId = $("#div5 .ChildC-input[child-checked]>:radio").attr("data-id");
    panelData._getChart(panelId, moduleId).dataSrc.check = $(this).attr("data-tag");
});

//获取数据
function getModuleData(chart) {
    var check = !chart.dataSrc.check ? "default" : chart.dataSrc.check;
    if (!chart.dataSrc[check]) {
        return;
    }
    var data = postModuleData(chart, check, false);
    chart.option = $.extend(true, {}, chart.option, data);
}

//获取地图Geojson
function getGeojson(code) {
    var mapJsonUrl = 'https://geo.datav.aliyun.com/areas_v2/bound/' + code + '_full.json';
    $.ajax({
        type: "get",
        url: mapJsonUrl,
        async: false,
        dataType: "json",
        success: function (data) {
            alert(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.error("请求地图Geojson数据失败")
        }
    });
}


$(function () {
    showLoading();
    //初始全部隐藏
    allHide();
    queryElement();
    queryPanel(getUrlParam("groupId"));
    closeLoading();
    setTimeout(function () {
        $("#div4 .Child-check:first input[name='tex']").click();
    }, 200);
});

/**
 * 元素的查询与渲染
 */
function queryElement() {
    $.ajax({
        type: "post",
        url: ctp + "/element/queryAll",
        cache: false,
        async: false,
        data: {},
        success: function (result) {
            if (result.code == "200") {
                var list = result.data;
                var allLiHtml = '';
                for (var i = 0; i < list.length; i++) {
                    var groupId = list[i].groupId;
                    var groupName = list[i].groupName;
                    var groupFileUrl = ctp + "/static/" + list[i].fileUrl;

                    var clist = list[i].children;
                    var elementFileUrl;
                    var elementName;
                    var ddHtml = "";
                    var liHtml = '<li class="layui-nav-item ">' +
                        '<a href="javascript:;">' +
                        '<img src="' + groupFileUrl + '" class="layui-nav-img">' +
                        '<p>' + groupName + '</p>' +
                        '</a>';

                    if (clist.length != 0) {
                        for (var j = 0; j < clist.length; j++) {
                            var elementGroupId = clist[j].groupId;
                            var elementId = clist[j].elementId;
                            var elementName = clist[j].elementName;
                            try {
                                var configJson = JSON.parse(clist[j].configJson);
                            } catch (err) {
                                var configJson = {
                                    error: "元素默认配置出错!请检查",
                                    text: clist[j].configJson + ""
                                };
                                console.error("解析出错:" + elementName + "err:" + err);
                            }
                            try {
                                var dataJson = JSON.parse(clist[j].dataJson);
                            } catch (err) {
                                var dataJson = {
                                    error: "元素默认配置出错!请检查",
                                    text: clist[j].dataJson + ""
                                };
                                console.error("解析出错:" + elementName + "err:" + err);
                            }
                            var showMenu = clist[j].showMenu.split(",");
                            var elementFileUrl = ctp + "/static/" + clist[j].fileUrl;
                            ddHtml +=
                                '<dd class="left element" data-element-id = "' + elementId + '" data-total="0">' +
                                '<a href="javascript:;">' +
                                '<img src="' + elementFileUrl + '" class="layui-nav-img">' +
                                '<p>' + elementName + '</p>' +
                                '</a>' +
                                '</dd>';
                            panelData._genElements(elementId, configJson, showMenu, dataJson);
                        }
                        var dl = '<dl class="layui-nav-child">' + ddHtml +
                            '</dl>' +
                            '</li>';
                        liHtml += dl;
                    } else {
                        liHtml += '</li>';
                    }
                    allLiHtml += liHtml;
                }
                $('.layui-layout-left').html(allLiHtml);

            } else {
                alert(result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("网络异常");
        }
    });
}

function queryPanel(groupId) {
    if (!groupId) {
        alert("参数非法！请重新进入");
    }
    //大屏的查询与渲染
    $.ajax({
        type: "post",
        url: ctp + "/panel/queryPanel", //  PanelGroupId 查询全部的 panel与 panelmodule
        cache: false,
        async: false,
        data: {
            "panelGroupId": groupId
        },
        success: function (result) {
            if (result.code == "200") {
                var panels = result.data;
                for (var i = 0; i < panels.length; i++) {
                    var panel = panels[i];
                    panelData._genPanel(panel.panelId, panel.panelName, panel.fileUrl, JSON.parse(panel.configJson));
                    panelListInit(panel.panelId);
                    var modules = panel.children;
                    if (modules) {
                        for (var j = 0; j < modules.length; j++) {
                            var module = modules[j];
                            panelData._genChart(panel.panelId, module.moduleId, module.elementId, null,
                                !module.configJson ? null : JSON.parse(module.configJson),
                                !module.moduleJson ? null : JSON.parse(module.moduleJson));
                        }
                    }
                }
            } else {
                alert(result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("网络异常");
        }
    });

}

/**
 * 大屏列表的渲染
 * @param id 大屏的id panelId
 */
function panelListInit(panelId) {
    var panel = panelData._getPanel(panelId);
    var panelName = panel.name;
    var fileUrl = ctp + "/static/" + panel.showSrc;
    var divHtml = '';
    divHtml =
        '<div class="Child-check" >' +
        '<div class="ChildC-input">' +
        '<input type="radio" name="tex" id="panel-' + panelId + '" data-id="' + panelId + '"   data-name = "' + panelName +
        '"class="input"><span></span>' +
        '</div>' +
        '<label for="panel-' + panelId + '">' +
        '<img src="' + fileUrl + '" alt="">' +
        '</label>' +
        '<div class="ChildC-fenl delPanel" data-id="' + panelId + '">' +
        '<img src="./../../assets/img/de.png" alt="">' +
        '</div>' +
        '</div>';
    $(".qieh").after(divHtml);
    $('#div4').show();
}

/**
 * 大屏设置的渲染
 * @param id 大屏的id panelId
 */
function panelOptionInit(panelId) {
    $('#div1').show();
    $('#div3').hide();
    var panel = panelData._getPanel(panelId);
    var option = !panel.option ? panelData._getDefaultOption("panel") : panel.option;
    $("#div1 input[name='p-width']").val(option.width);
    $("#div1 input[name='p-height']").val(option.height);
    $("#div1 input[name='pu'][value='" + (!option.zoomType ? "2" : option.zoomType) + "']").click();
    $("#div1 input[name='zhi'][value='" + (!option.coverSetting ? "0" : option.coverSetting) + "']").click();
    $("#div1 .ChildT-bj .layui-tab-title li.layui-this").removeClass("layui-this");
    $("#div1 .ChildT-bj .layui-tab-content .layui-tab-item.layui-show").removeClass("layui-show");
    if (option.backgroundType == "0") {
        $("#div1 .ChildT-bj .layui-tab-title li:eq(0)").addClass("layui-this");
        $("#div1 .ChildT-bj .layui-tab-content .layui-tab-item:eq(0)").addClass("layui-show");
        //0 背景图
        $('#container').css('background-image', 'url(' + option.backgroundSrc + ')');
        $('#avarimgs').attr('src', option.backgroundSrc);
        $('#container').css('background-repeat', "no-repeat");
        //0等比缩放宽度铺满 1等比缩放高度铺满 2全屏铺满 3原始像素
        if (option.zoomType == "0") {
            $('#container').css('background-size', option.width + "px auto");
        } else if (option.zoomType == "1") {
            $('#container').css('background-size', "auto " + option.height + "px");
        } else if (option.zoomType == "2") {
            $('#container').css('background-size', "100% 100%");
        } else if (option.zoomType == "3") {
            $('#container').css('background-size', option.width + "px " + option.height + "px");
        }
    } else {
        $("#div1 .ChildT-bj .layui-tab-title li:eq(1)").addClass("layui-this");
        $("#div1 .ChildT-bj .layui-tab-content .layui-tab-item:eq(1)").addClass("layui-show");
        // 1背景色
        var backgroundSrc = !option.backgroundSrc ? "#0d2b5e" : option.backgroundSrc;
        $('#container').css('background-color', backgroundSrc);
        renderColorpicker('#mycolor', backgroundSrc, function (color) {
            $('#container').css('background-color', color);
            panelOptionSync();
        });
    }
    $("#div1  input[name='color'][value='" + option.backgroundType + "']").click();
}

/**
 * 大屏设置的同步
 */
function panelOptionSync() {
    var id = $("#div4 .ChildC-input[child-checked]>:radio").attr("data-id");
    var backgroundType = $("#div1 .ChildT-bj input[name='color']:checked").val();
    var backgroundSrc = "#0d2b5e";
    if (backgroundType == '0') {
        backgroundSrc = $("#div1 #avarimgs").attr("src");
    } else {
        backgroundSrc = $("#div1 #mycolor").attr("data-color");
    }
    var coverSetting = $("#div1 .ChildT-bj input[name='zhi']").val();
    if (coverSetting == "0") {
        getCover(id);
    } else {
        panelData._getPanel(id).showSrc = $("#div1 #CoverPng").attr("src");
    }
    //0 背景图 1背景色
    var option = {
        width: $("#div1  input[name='p-width']").val(),
        height: $("#div1 input[name='p-height']").val(),
        coverSetting: coverSetting,
        backgroundType: backgroundType,
        backgroundSrc: backgroundSrc,
        zoomType: $("#div1 input[name='pu']:checked").val()
    }
    panelData._getPanel(id).option = option;
}

/**
 * 组件列表渲染
 * @param id 大屏的id panelId
 */
function moduleListInit(panelId, moduleId) {
    var chart = panelData._getChart(panelId, moduleId);
    //左侧列表的渲染
    var src = $(".layui-layout-left .element[data-element-id='" + chart.elementId + "'] a img").attr("src");
    var listHtml =
        '<div class="Child-check">' +
        '<div class="ChildC-input">' +
        '<input type="radio" name="mod" id="module-' + moduleId + '" data-id="' + moduleId + '" element-id = "' + chart.elementId +
        '"class="input"><span></span>' +
        '</div>' +
        '<label for="module-' + moduleId + '"><img src="' + src + '" alt=""> </label>' +
        '<div class="ChildC-fenl"><img src="./../../assets/img/fenlei.png" alt=""></div>' +
        '</div>';

    $('#moduleData').after(listHtml);
    //右侧设置的渲染
    $.ajax({
        type: "get",
        url: "../../data/menu.sc",
        async: false,
        dataType: "text",
        success: function (data) {
            $("#div2 #Menv").html(data);
            $("#Menv .menu-item").hide();
            $(".Menvtilte").show();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.error("请求menu.sc数据失败")
        }
    });
}

/**
 * 组件渲染
 * @param  panelId 大屏id
 * @param  moduleId 组件id
 */
function moduleInit(panelId, moduleId) {
    var chart = panelData._getChart(panelId, moduleId);
    var pos = chart.offset;
    var w = Math.floor($("#container").width() * pos.width)
    var h = Math.floor($("#container").height() * pos.height)
    var l = Math.floor($("#container").width() * pos.left)
    var t = Math.floor($("#container").height() * pos.top)
    var html = '<div class="dragable" id="' + moduleId + '"  panel-id="' + panelId + '" element-id="' + chart.elementId + '"  data-ClienX="" data-ClienY="" style="position: absolute;left:' +
        l + 'px; top: ' + t + 'px;  height: ' + h + 'px; width: ' + w + 'px;"  tabindex="1">' +
        '<div class="contt" style="height:100%;height:100%;"></div>' +
        '</div>';
    $('#container').append(html);
    var echartIns = echarts.init($("#" + moduleId).children(".contt")[0]);
    echartIns.setOption(chart.option);
    addModuleEvents();
    moduleOptionInit(panelId, moduleId);
    chart.echartIns = echartIns;
    return echartIns;
}

/**
 * 组件设置区样式渲染
 * @param  panelId 大屏id
 * @param  moduleId 组件id
 */
function moduleOptionInit(panelId, moduleId) {
    layui.use(['layer', 'element', 'form', 'colorpicker', 'layarea'], function () {
        var form = layui.form;
        var element = layui.element;
        var layer = layui.layer;
        var colorpicker = layui.colorpicker;
        var layarea = layui.layarea;
        element.render();//折叠面板更新渲染
        form.render();//表单更新渲染
        layarea.render({
            elem: "#area-picker",
            data: {
                province: '上海市', //省
                city: '上海市', //市
                county: '宝山区', //区
            },
            change: function (res) {
                console.log(res);
            }
        });
        //开关
        form.on('switch(MenuSwitch)', function (data) {
            var num = $(data.elem).closest(".menu-item[show]").attr("show");
            $(data.elem).attr('data-check', data.elem.checked);
            moduleOptionSync(num);
        });
    });

    var chart = panelData._getChart(panelId, moduleId);
    var showMenu = panelData._getElement(chart.elementId).showMenu;
    var dataSrc = chart.dataSrc;
    for (var i in showMenu) {
        var showNum = showMenu[i];
        setModuleMenuOption(showNum, chart);
    }
    var check = "default";
    for (var d in dataSrc) {
        if (d == "error") {
            continue;
        } else if (d == "check" && dataSrc[d]) {
            check = dataSrc[d];
            break;
        }
    }
    $(".datasource-lis button[data-tag='" + check + "']").addClass();
}

// input改变事件
$('#div2').on('change', 'input[type=text]', function () {
    var num = $(this).closest(".menu-item[show]").attr("show");
    moduleOptionSync(num);
})

// select改变事件
$('#div2').on('change', 'select', function () {
    var num = $(this).closest(".menu-item[show]").attr("show");
    moduleOptionSync(num);
})

// 只需输入数字
$('#div2').on('keyup', 'input.dataNumber', function () {
    var tmptxt = $(this).val().replace(/\D|^0/g, '');
    $(this).val(tmptxt);
})
// 只需输入数字和小数
$('#div2').on('keyup', 'input.dataDecimal', function () {
    var tmptxt = $(this).val().replace(/^\D*(\d*(?:\.\d{0,1})?).*$/g, '$1');
    $(this).val(tmptxt);
})
// 只需输入数字和负号
$('#div2').on('keyup', 'input.dataMinus', function () {
    var tmptxt = $(this).val().replace(/[^0-9.-]/g, '');
    $(this).val(tmptxt);
})

/**
 * 组件单个设置同步
 */
function moduleOptionSync(num) {
    var panelId = $("#Menv").attr("panel-id");
    var elementId = $("#Menv").attr("element-id");
    var moduleId = $("#Menv").attr("module-id");
    var key = $("#div2 #Menv .layui-colla-item[show='" + num + "']").attr("option-name");
    var chart = panelData._getChart(panelId, moduleId);
    var temp = {};
    temp[key] = getModuleMenuOption(num);
    chart.option = $.extend(true, chart.option, temp);
    getModuleData(chart);
    chart.echartIns.setOption(chart.option);
    chart.echartIns.resize();
}


/**
 * 组件所有设置同步
 */
function moduleOptionSyncAll() {
    var panelId = $("#Menv").attr("panel-id");
    var elementId = $("#Menv").attr("element-id");
    var moduleId = $("#Menv").attr("module-id");
    var option = {};
    $("#div2 #Menv").find(".layui-colla-item[show]").each(function () {
        var num = $(this).attr("show");
        var key = $(this).attr("option-name");
        option[key] = getModuleMenuOption(num);
    });
    var chart = panelData._getChart(panelId, moduleId);
    chart.option = $.extend(true, {}, chart.option, option);
    getModuleData(chart);
    chart.echartIns.setOption(chart.option);
    chart.echartIns.resize();
}

//获取设置区的设置
function getModuleMenuOption(num) {
    var number = Number(num);
    var option = {};
    switch (number) {
        case 1:
            option.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='fontSize']").val();
            option.fontFamily = $(".layui-colla-item[show='" + num + "'] select[option-name='fontFamily']").val();
            option.fontWeight = $(".layui-colla-item[show='" + num + "'] select[option-name='fontWeight']").val();
            option.color = $(".layui-colla-item[show='" + num + "'] div[option-name='color']").attr("data-color");
            break;
        case 2:
            option.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='show']").attr('data-check'));
            option.text = $(".layui-colla-item[show='" + num + "'] input[option-name='text']").val();
            option.left = $(".layui-colla-item[show='" + num + "'] select[option-name='left']").val();
            option.top = $(".layui-colla-item[show='" + num + "'] select[option-name='top']").val();
            var textStyle = {};
            textStyle.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val();
            textStyle.color = $(".layui-colla-item[show='" + num + "'] div[option-name='textStyle.color']").attr("data-color");
            option.textStyle = textStyle;
            option.link = $(".layui-colla-item[show='" + num + "'] select[option-name='link']").val();
            option.target = $(".layui-colla-item[show='" + num + "'] select[option-name='target']").val();
            break;
        case 3:
            var textStyle = {};
            option.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='show']").attr('data-check'));
            option.itemWidth = $(".layui-colla-item[show='" + num + "'] input[option-name='itemWidth']").val();
            option.itemHeight = $(".layui-colla-item[show='" + num + "'] input[option-name='itemHeight']").val();
            option.orient = $(".layui-colla-item[show='" + num + "'] select[option-name='orient']").val();
            option.left = $(".layui-colla-item[show='" + num + "'] select[option-name='left']").val();
            option.top = $(".layui-colla-item[show='" + num + "'] select[option-name='top']").val();
            textStyle.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val();
            textStyle.color = $(".layui-colla-item[show='" + num + "'] div[option-name='textStyle.color']").attr("data-color");
            option.textStyle = textStyle;
            break;
        case 4:
            option.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='show']").attr('data-check'));
            break;
        case 5:
            var axisTick = {},
                splitLine = {},
                axisLabel = {},
                axisLine = {
                    lineStyle: {},
                },
                lineStyle3 = {};
            option.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='show']").attr('data-check'));
            axisLine.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='axisLine.show']").attr('data-check'));
            option.inverse = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='inverse']").attr('data-check'));
            axisTick.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='axisTick.show']").attr('data-check'));
            splitLine.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='splitLine.show']").attr('data-check'));
            option.min = $(".layui-colla-item[show='" + num + "'] input[option-name='min']").val();
            option.max = $(".layui-colla-item[show='" + num + "'] input[option-name='max']").val();
            axisLabel.interval = $(".layui-colla-item[show='" + num + "'] select[option-name='axisLabel.interval']").val();
            axisLabel.rotate = $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.rotate']").val();
            option.name = $(".layui-colla-item[show='" + num + "'] input[option-name='name']").val();
            axisLabel.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.fontSize']").val();
            lineStyle3.color = $(".layui-colla-item[show='" + num + "'] div[option-name='axisLine.lineStyle.color']").attr("data-color");
            axisLabel.color = $(".layui-colla-item[show='" + num + "'] div[option-name='axisLabel.color']").attr("data-color");
            option.axisLabel = axisLabel;
            option.splitLine = splitLine;
            option.axisTick = axisTick;
            axisLine.lineStyle = lineStyle3;
            option.axisLine = axisLine;

            break;
        case 6:
            var axisTick = {},
                splitLine = {},
                axisLabel = {},
                axisLine = {
                    lineStyle3: {},
                },
                lineStyle3 = {};
            option.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='show']").attr('data-check'));
            axisLine.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='axisLine.show']").attr('data-check'));
            option.inverse = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='inverse']").attr('data-check'));
            axisTick.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='axisTick.show']").attr('data-check'));
            splitLine.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='splitLine.show']").attr('data-check'));
            option.min = $(".layui-colla-item[show='" + num + "'] input[option-name='min']").val();
            option.max = $(".layui-colla-item[show='" + num + "'] input[option-name='max']").val();
            axisLabel.interval = $(".layui-colla-item[show='" + num + "'] select[option-name='axisLabel.interval']").val();
            axisLabel.rotate = $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.rotate']").val();
            option.name = $(".layui-colla-item[show='" + num + "'] input[option-name='name']").val();
            axisLabel.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.fontSize']").val();
            lineStyle3.color = $(".layui-colla-item[show='" + num + "'] div[option-name='axisLine.lineStyle.color']").attr("data-color");
            axisLabel.color = $(".layui-colla-item[show='" + num + "'] div[option-name='axisLabel.color']").attr("data-color");
            option.axisLabel = axisLabel;
            option.splitLine = splitLine;
            option.axisTick = axisTick;
            axisLine.lineStyle = lineStyle3;
            option.axisLine = axisLine;
            break;
        case 7:
            var bgc = $(".layui-colla-item[show='" + num + "'] div[option-name='backgroundColor']").attr("data-color").toString();
            option = bgc;
            break;
        case 8:
            var series = [];
            for (var i = 0; i < 1; i++) {
                series.push({
                    itemStyle: {
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.color']").attr("data-color"),
                        barBorderRadius: $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.barBorderRadius']").val()
                    },
                    barGap: $(".layui-colla-item[show='" + num + "'] input[option-name='barGap']").val(),
                    barWidth: $(".layui-colla-item[show='" + num + "'] input[option-name='barWidth']").val()
                })
            }
            option = series;
            break;
        case 9:
            var series = [];
            for (var i = 0; i < 1; i++) {
                series.push({
                    symbolSize: $(".layui-colla-item[show='" + num + "'] input[option-name='symbolSize']").val(),
                    itemStyle: {
                        borderColor: $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.borderColor']").attr("data-color"),
                        borderWidth: $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.borderWidth']").val(),
                    },
                    lineStyle: {
                        type: $(".layui-colla-item[show='" + num + "'] select[option-name='lineStyle.type']").val(),
                        width: $(".layui-colla-item[show='" + num + "'] input[option-name='lineStyle.width']").val(),
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='lineStyle.color']").attr("data-color"),
                    },
                    smooth: $(".layui-colla-item[show='" + num + "'] input[option-name='smooth']").val()
                })
            }
            option = series;
            break;
        case 13:
            option.center = [
                ($(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(0)").val() + '%'),
                ($(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(1)").val() + '%'),
            ];
            option.radius = ($(".layui-colla-item[show='" + num + "'] input[option-name='radius']").val() + '%');
            break;
        case 15:
            var series = [];
            for (var i = 0; i < 1; i++) {
                series.push({
                    itemStyle: {
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.color']:eq(" + i + ")").attr("data-color")
                    },
                    symbolSize: $(".layui-colla-item[show='" + num + "'] input[option-name='symbolSize']:eq(" + i + ")").val()
                })
            }
            option = series;
            break;
        case 16:
            var series = [];
            for (var i = 0; i < 2; i++) {
                series.push({
                    center: [
                        ($(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(0)").val() + '%'),
                        ($(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(1)").val() + '%'),
                    ],
                    radius: [
                        ($(".layui-colla-item[show='" + num + "'] input[option-name='radius']:eq(0)").val() + '%'),
                        ($(".layui-colla-item[show='" + num + "'] input[option-name='radius']:eq(1)").val() + '%'),
                    ],
                    label: {
                        show: JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='label.show']").attr('data-check')),
                        position: $(".layui-colla-item[show='" + num + "'] select[option-name='label.position']").val(),
                    },
                    labelLine: {
                        show: JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='labelLine.show']").attr('data-check')),
                    },
                    emphasis: {
                        label: {
                            show: JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.show']").attr('data-check')),
                            fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.fontSize']").val(),
                        }
                    },
                    data: [{
                        itemStyle: {
                            color: $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.color']").attr("data-color")
                        }
                    }]
                })
            }
            option = series;
            break;
        case 18:
            var series = [];
            for (var i = 0; i < 0; i++) {
                series.push({
                    label: {
                        show: JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='label.show']").attr('data-check')),
                        fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='label.fontSize']").val(),
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='label.color']").attr("data-color"),
                    },
                    emphasis: {
                        label: {
                            show: JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.show']").attr('data-check')),
                            fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.fontSize']").val(),
                            color: $(".layui-colla-item[show='" + num + "'] div[option-name='emphasis.label.color']").attr("data-color"),
                        }
                    }
                })
            }
            option = series;
            break;
        case 19:
            var series = [];
            for (var i = 0; i < 0; i++) {
                series.push({
                    min: $(".layui-colla-item[show='" + num + "'] input[option-name='min']").val(),
                    max: $(".layui-colla-item[show='" + num + "'] input[option-name='max']").val(),
                    startAngle: $(".layui-colla-item[show='" + num + "'] input[option-name='startAngle']").val(),
                    endAngle: $(".layui-colla-item[show='" + num + "'] input[option-name='endAngle']").val(),
                    title: {
                        fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='title.fontSize']").val(),
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='title.color']").attr("data-color")
                    },
                    detail: {
                        fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='detail.fontSize']").val(),
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='detail.color']").attr("data-color"),
                    },
                    axisLabel: {
                        fontSize: $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.fontSize']").val(),
                        color: $(".layui-colla-item[show='" + num + "'] div[option-name='axisLabel.color']").attr("data-color")
                    }
                })
            }
            option = series;
            break;
        case 20:
            option.type = 'map';
            var itemStyle = {},
                label = {},
                emphasis = {
                    label: {},
                    itemStyle: {},
                },
                label2 = {},
                itemStyle2 = {};
            option.roam = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='roam']").attr('data-check'));
            option.zoom = $(".layui-colla-item[show='" + num + "'] input[option-name='zoom']").val();
            option.boundingCoords = $(".layui-colla-item[show='" + num + "'] input[option-name='boundingCoords']").val();
            option.slient = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='slient']").attr('data-check'));
            label.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='label.show']").attr('data-check'));
            itemStyle.borderWidth = $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.borderWidth']").val();
            itemStyle.borderColor = $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.borderColor']").attr("data-color");
            itemStyle.areaColor = $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.areaColor']").attr("data-color");
            itemStyle.shadowColor = $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.shadowColor']").attr("data-color");
            itemStyle.shadowBlur = $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowBlur']").val();
            itemStyle.shadowOffsetX = $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowOffsetX']").val();
            itemStyle.shadowOffsetY = $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowOffsetY']").val();
            label2.show = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.show']").attr('data-check'));
            label2.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.fontSize']").val();
            label2.color = $(".layui-colla-item[show='" + num + "'] div[option-name='emphasis.label.color']").attr("data-color");
            itemStyle2.borderWidth = $(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.itemStyle.borderWidth']").val();
            itemStyle2.borderColor = $(".layui-colla-item[show='" + num + "'] div[option-name='emphasis.itemStyle.borderColor']").attr("data-color");
            itemStyle2.areaColor = $(".layui-colla-item[show='" + num + "'] div[option-name='emphasis.itemStyle.areaColor']").attr("data-color");
            emphasis.itemStyle = itemStyle2;
            emphasis.label = label2;
            option.label = label;
            option.itemStyle = itemStyle;
            option.emphasis = emphasis;
            break;
        case 21:

            break;
        case 22:

            break;
        case 23:

            break;
        case 24:

            break;
        case 25:

            break;
        case 27:
            var textStyle = {},
                axisPointer = {
                    lineStyle: {}
                },
                lineStyle3 = {};
            option.trigger = $(".layui-colla-item[show='" + num + "'] select[option-name='trigger']").val();
            textStyle.fontSize = $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val();
            textStyle.color = $(".layui-colla-item[show='" + num + "'] div[option-name='textStyle.color']").attr("data-color");
            axisPointer.type = $(".layui-colla-item[show='" + num + "'] select[option-name='axisPointer.type']").val();
            lineStyle3.width = $(".layui-colla-item[show='" + num + "'] input[option-name='axisPointer.lineStyle.width']").val();
            lineStyle3.color = $(".layui-colla-item[show='" + num + "'] div[option-name='axisPointer.lineStyle.color']").attr("data-color");
            axisPointer.lineStyle = lineStyle3;
            option.axisPointer = axisPointer;
            option.textStyle = textStyle;
            break;
        case 28:
            option.containLabel = JSON.parse($(".layui-colla-item[show='" + num + "'] input[option-name='containLabel']").attr('data-check'));
            option.left = ($(".layui-colla-item[show='" + num + "'] input[option-name='left']").val() + '%');
            option.right = ($(".layui-colla-item[show='" + num + "'] input[option-name='right']").val() + '%');
            option.top = ($(".layui-colla-item[show='" + num + "'] input[option-name='top']").val() + '%');
            option.bottom = ($(".layui-colla-item[show='" + num + "'] input[option-name='bottom']").val() + '%');

            break;
        case 30:

            break;
    }
    return option;
}

//按照数据渲染设置区
function setModuleMenuOption(num, chart) {
    var number = Number(num);
    var option = chart.option;
    switch (number) {
        case 1:
            $(".layui-colla-item[show='" + num + "'] input[option-name='fontSize']").val(option.textStyle.fontSize);
            $(".layui-colla-item[show='" + num + "'] select[option-name='fontFamily']").val(option.textStyle.fontFamily);
            $(".layui-colla-item[show='" + num + "'] select[option-name='fontWeight']").val(option.textStyle.fontWeight);
            renderColorpicker("#textColor", option.textStyle.color, function () {
                moduleOptionSync(num);
            });
            break;
        case 2:
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='show']", option.title.show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='text']").val(option.title.text);
            $(".layui-colla-item[show='" + num + "'] select[option-name='left']").val(option.title.left);
            $(".layui-colla-item[show='" + num + "'] select[option-name='top']").val(option.title.top);
            $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val(option.title.textStyle.fontSize);
            renderColorpicker("#inputColor", option.textStyle.color, function () {
                moduleOptionSync(num);
            });
            $(".layui-colla-item[show='" + num + "'] select[option-name='link']").val(option.title.link);
            $(".layui-colla-item[show='" + num + "'] select[option-name='target']").val(option.title.target);
            break;
        case 3:
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='show']", option.legend.show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemWidth']").val(option.legend.itemWidth);
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemHeight']").val(option.legend.itemHeight);
            $(".layui-colla-item[show='" + num + "'] select[option-name='orient']").val(option.legend.orient);
            $(".layui-colla-item[show='" + num + "'] select[option-name='left']").val(option.legend.left);
            $(".layui-colla-item[show='" + num + "'] select[option-name='top']").val(option.legend.top);
            $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val(option.legend.textStyle.fontSize);
            renderColorpicker("#legentColor", option.textStyle.color, function () {
                moduleOptionSync(num);
            });
            break;
        case 4:
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='show']", option.dataZoom[0].show);
            // $().attr('data-check', );
            break;
        case 5:
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='show']", option.xAxis.show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='axisLine.show']", empty2default(option.xAxis.axisLine, {show: true}).show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='inverse']", option.xAxis.inverse);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='axisTick.show']", empty2default(option.xAxis.axisTick, {show: true}).show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='splitLine.show']", empty2default(option.xAxis.splitLine, {show: true}).show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='min']").val(option.xAxis.min);
            $(".layui-colla-item[show='" + num + "'] input[option-name='max']").val(option.xAxis.max);
            $(".layui-colla-item[show='" + num + "'] select[option-name='axisLabel.interval']").val(empty2default(option.xAxis.axisLabel, {interval: 0}).interval);
            $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.rotate']").val(empty2default(option.xAxis.axisLabel, {rotate: 0}).rotate);
            $(".layui-colla-item[show='" + num + "'] input[option-name='name']").val(option.xAxis.name);
            $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.fontSize']").val(empty2default(option.xAxis.axisLabel, {fontSize: 14}).fontSize);
            renderColorpicker("#xAxisLineColor", empty2default(option.xAxis.axisLabel, {lineStyle: {color: '#eee'}}).color, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#xAxisLabelColor", empty2default(option.xAxis.axisLabel, {color: '#eee'}).color, function () {
                moduleOptionSync(num);
            });
            break;
        case 6:
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='show']", option.yAxis.show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='axisLine.show']", empty2default(option.yAxis.axisLine, {show: true}).show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='inverse']", option.yAxis.inverse);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='axisTick.show']", empty2default(option.yAxis.axisTick, {show: true}).show);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='splitLine.show']", empty2default(option.yAxis.splitLine, {show: true}).show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='min']").val(option.yAxis.min);
            $(".layui-colla-item[show='" + num + "'] input[option-name='max']").val(option.yAxis.max);
            $(".layui-colla-item[show='" + num + "'] select[option-name='axisLabel.interval']").val(empty2default(option.yAxis.axisLabel, {interval: 0}).interval);
            $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.rotate']").val(empty2default(option.yAxis.axisLabel, {rotate: 0}).rotate);
            $(".layui-colla-item[show='" + num + "'] input[option-name='name']").val(option.yAxis.name);
            $(".layui-colla-item[show='" + num + "'] input[option-name='axisLabel.fontSize']").val(empty2default(option.yAxis.axisLabel, {fontSize: 14}).fontSize);
            renderColorpicker("#yAxisLineColor", empty2default(option.yAxis.axisLabel, {lineStyle: {color: '#eee'}}).color, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#yAxisLabelColor", empty2default(option.yAxis.axisLabel, {color: '#eee'}).color, function () {
                moduleOptionSync(num);
            });
            break;
        case 7:
            renderColorpicker("#backGroundColor", option.backgroundColor, function () {
                moduleOptionSync(num);
            });
            break;
        case 8:
            setDataColor(num, chart);
            layui.use('element', function () {
                var element = layui.element;
                element.render();
            })
            break;
        case 9:

            setDataColor(num, chart);
            layui.use('element', function () {
                var element = layui.element;
                element.render();
            })
            break;
        case 13:
            $(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(0)").val((empty2default(option.radar.center, "50").replace('%', '')));
            $(".layui-colla-item[show='" + num + "'] input[option-name='center']:eq(1)").val((empty2default(option.radar.center, "50").replace('%', '')));
            $(".layui-colla-item[show='" + num + "'] input[option-name='radius']").val((empty2default(option.radar.radius, "80").replace('%', '')));
            break;
        case 14:
            $(".layui-colla-item[show='" + num + "'] input[option-name='symbolSize']").val(option.series.symbolSize);
            break;
        case 15:
            //未在页面找到此选择器
            // $(".layui-colla-item[show='" + num + "'] div[option-name='itemStyle.color']").attr("data-color", empty2default(option.series.itemStyle, {color: "#fff"}).color);
            setDataColor(num, chart);
            break;
        case 16:
            setDataColor(num, chart)
            layui.use(['element', 'form'], function () {
                var element = layui.element;
                var form = layui.form;
                element.render();
                form.render()
            })
            break;
        case 18:
            setDataColor(num, chart);
            layui.use(['element', 'form'], function () {
                var element = layui.element;
                var form = layui.form;
                element.render();
                form.render()
            })
            break;
        case 19:
            setDataColor(num, chart)
            break;
        case 20:
            $(".layui-colla-item[show='" + num + "'] input[option-name='roam']").attr('data-check', option.series.roam);
            $(".layui-colla-item[show='" + num + "'] input[option-name='zoom']").val(option.series.zoom);
            $(".layui-colla-item[show='" + num + "'] input[option-name='boundingCoords']").val(option.series.boundingCoords);
            $(".layui-colla-item[show='" + num + "'] input[option-name='slient']").attr('data-check', option.series.slient);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='label.show']", empty2default(option.series.label, {show: false}).show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.borderWidth']").val(empty2default(option.series.itemStyle, {borderWidth: 1}).borderWidth);
            renderColorpicker("#MapBorderColor", empty2default(option.series.itemStyle, {borderColor: '#a5fecb'}).borderColor, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#MapAreaColor", empty2default(option.series.itemStyle, {areaColor: "#20bdff"}).areaColor, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#MapShadowColor", empty2default(option.series.itemStyle, {shadowColor: '#000'}).shadowColor, function () {
                moduleOptionSync(num);
            });
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowBlur']").val(empty2default(option.series.itemStyle, {shadowBlur: 0}).shadowBlur);
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowOffsetX']").val(empty2default(option.series.itemStyle, {shadowOffsetX: 0}).shadowOffsetX);
            $(".layui-colla-item[show='" + num + "'] input[option-name='itemStyle.shadowOffsetY']").val(empty2default(option.series.itemStyle, {shadowOffsetY: 0}).shadowOffsetY);
            renderForm(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.show']", empty2default(option.series.emphasis, {label: {show: false}}).show);
            $(".layui-colla-item[show='" + num + "'] input[option-name='emphasis.label.fontSize']").val(empty2default(option.series.emphasis, {label: {fontSize: 14}}).fontSize);
            renderColorpicker("#MapEmpLabelColor", empty2default(option.series.emphasis, {label: {color: '#eee'}}).color, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#MapEmpBorderColor", empty2default(option.series.emphasis, {itemStyle: {borderColor: '#000'}}).borderColor, function () {
                moduleOptionSync(num);
            });
            renderColorpicker("#MapEmpAraeColor", empty2default(option.series.emphasis, {itemStyle: {areaColor: '#dc2424'}}).areaColor, function () {
                moduleOptionSync(num);
            });
            break;
        case 21:

            break;
        case 22:

            break;
        case 23:

            break;
        case 24:

            break;
        case 25:

            break;
        case 27:
            $(".layui-colla-item[show='" + num + "'] select[option-name='trigger']").val(option.tooltip.trigger);
            $(".layui-colla-item[show='" + num + "'] input[option-name='textStyle.fontSize']").val(option.tooltip.textStyle.fontSize);
            renderColorpicker("#ToolTipTextColor", option.tooltip.textStyle.color, function () {
                moduleOptionSync(num);
            });
            $(".layui-colla-item[show='" + num + "'] select[option-name='axisPointer.type']").val(option.tooltip.axisPointer.type);
            $(".layui-colla-item[show='" + num + "'] input[option-name='axisPointer.lineStyle.width']").val(option.tooltip.axisPointer.lineStyle.width);
            renderColorpicker("#ToolTipLineColor", option.tooltip.axisPointer.lineStyle.color, function () {
                moduleOptionSync(num);
            });
            break;
        case 28:
            $(".layui-colla-item[show='" + num + "'] input[option-name='containLabel']").attr('data-check', option.grid.containLabel);
            $(".layui-colla-item[show='" + num + "'] input[option-name='left']").val((option.grid.left).replace('%', ''));
            $(".layui-colla-item[show='" + num + "'] input[option-name='right']").val((option.grid.right).replace('%', ''));
            $(".layui-colla-item[show='" + num + "'] input[option-name='top']").val((option.grid.top).replace('%', ''));
            $(".layui-colla-item[show='" + num + "'] input[option-name='bottom']").val((option.grid.bottom).replace('%', ''));

            break;
        case 30:

            break;
    }
    return option;
}

function empty2default(obj, def) {
    if (obj instanceof Array) {
        return obj[0];
    } else if (!obj) {
        return def;
    } else {
        return obj;
    }
}


function setDataColor(num, chart) {
    var elementId = Number(chart.elementId);
    var option = chart.option;
    switch (elementId) {
        case 26:
            $(".menu-item[show=15]").find('.layui-colla-content').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=15]").find('.layui-colla-content').append(
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md4'>" + series[s].name + "</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='ItemStyleColor" + [s] + "' option-name='itemStyle.color' data-color='" + series[s].itemStyle.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md4'>气泡大小</div>" +
                    "<div class='layui-col-md6'>" +
                    "<input type='text' value='" + series[s].symbolSize + "' class='dataNumber'  option-name='symbolSize'>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#ItemStyleColor" + [s] + "", series[s].itemStyle.color, function () {
                    moduleOptionSync(num);
                });
            }
            break;
        case 22:
        case 21:
            $(".menu-item[show=15]").find('.layui-colla-content').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=15]").find('.layui-colla-content').append(
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md4'>" + series[s].name + "</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='ItemStyleColor" + [s] + "' option-name='itemStyle.color' data-color='" + series[s].itemStyle.color + "'></div>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#ItemStyleColor" + [s] + "", series[s].itemStyle.color, function () {
                    moduleOptionSync(num);
                });
            }

            break;
        case 19:
        case 20:
            $(".menu-item[show=9]").find('.layui-collapse').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=9]").find('.layui-collapse').append(
                    "<div class='layui-colla-item'>" +
                    "<h2 class='layui-colla-title'>" + series[s].name + "</h2>" +
                    "<div class='layui-colla-content'>" +
                    "<div class='layui-fluid'>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>拐点大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].symbolSize + "' class='dataNumber'  option-name='symbolSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>拐点描边宽度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].itemStyle.borderWidth + "' class='dataNumber'  option-name='itemStyle.borderWidth'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>拐点描边颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='LineBorderColor" + [s] + "' option-name='itemStyle.borderColor' data-color='" + series[s].itemStyle.borderColor + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md4'>折线样式</div>" +
                    "<div class='layui-col-md8'>" +
                    "<select option-name='lineStyle.type'>" +
                    "<option value='solid' " + ((series[s].lineStyle.type == 'solid') ? 'selected' : ' ') + ">实线</option>" +
                    "<option value='dashed' " + ((series[s].lineStyle.type == 'dashed') ? 'selected' : ' ') + ">虚线</option>" +
                    "<option value='dotted' " + ((series[s].lineStyle.type == 'dotted') ? 'selected' : ' ') + ">点状</option>" +
                    "</select>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>折线宽度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].lineStyle.width + "' class='dataNumber'  option-name='lineStyle.width'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>折线颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='LineStyleColor" + [s] + "' option-name='lineStyle.color' data-color='" + series[s].lineStyle.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md4'>平滑度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].smooth + "' class='dataDecimal'  option-name='smooth'>" +
                    "</div>" +
                    "<div class='layui-col-md3' style='line-height: 18px'>0-1之间</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#LineBorderColor" + [s] + "", series[s].itemStyle.borderColor, function () {
                    moduleOptionSync(num);
                });
                renderColorpicker("#LineStyleColor" + [s] + "", series[s].lineStyle.color, function () {
                    moduleOptionSync(num);
                });
            }
            break;
        case 15:
        case 16:
        case 17:
        case 18:
            $(".menu-item[show=8]").find('.layui-collapse').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=8]").find('.layui-collapse').append(
                    "<div class='layui-colla-item'>" +
                    "<h2 class='layui-colla-title'>" + series[s].name + "</h2>" +
                    "<div class='layui-colla-content'>" +
                    "<div class='layui-fluid'>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>系列颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='BarItemColor" + [s] + "' option-name='itemStyle.color' data-color='" + series[s].itemStyle.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'><i class='iconfont'>&#xe616;</i><span>不同系列柱间间距</span></div>" +
                    "<div class='layui-col-md7'>" +
                    "<input type='text' value='" + series[s].barGap + "' class='dataNumber'  option-name='barGap'>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>柱条宽度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].barWidth + "' class='dataNumber'  option-name='barWidth'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>柱条圆角</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].itemStyle.barBorderRadius + "' class='dataNumber'  option-name='itemStyle.barBorderRadius'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#BarItemColor" + [s] + "", series[s].itemStyle.color, function () {
                    moduleOptionSync(num);
                });
            }
            break;
        case 6:
            $(".menu-item[show=15]").find('.layui-colla-content').empty();
            var series = option.series;
            for (var s in series) {
                var data = series[s].data;
                for (var d in data) {
                    $(".menu-item[show=15]").find('.layui-colla-content').append(
                        "<div class='layui-row'>" +
                        "<div class='layui-col-md5' style='line-height: 18px'>" + data[d].name + "</div>" +
                        "<div class='layui-col-md3'>" +
                        "<div class='mycolor' id='ItemStyleColor" + [d] + "' option-name='itemStyle.color' data-color='" + data[d].lineStyle.color + "'></div>" +
                        "</div>" +
                        "</div>")
                    renderColorpicker("#ItemStyleColor" + [d] + "", data[d].lineStyle.color, function () {
                        moduleOptionSync(num);
                    });
                }

            }
            break;
        case 4:
            $(".menu-item[show=19]").find('.layui-fluid').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=19]").find('.layui-fluid').append(
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>仪表最小值</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].min + "' class='dataMinus'  option-name='min'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>仪表最大值</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].max + "' class='dataNumber'  option-name='max'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>起始角度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].startAngle + "' class='dataMinus' option-name='startAngle'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>度</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>结束角度</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].endAngle + "' class='dataMinus' option-name='endAngle'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>度</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>标题大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].title.fontSize + "' class='dataNumber'  option-name='title.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>标题颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='TitleColor" + [s] + "' option-name='title.color' data-color='" + series[s].title.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>详情大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].detail.fontSize + "' class='dataNumber'  option-name='detail.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>详情颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='DetailColor" + [s] + "' option-name='detail.color' data-color='" + series[s].detail.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>刻度标签大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].axisLabel.fontSize + "' class='dataNumber'  option-name='axisLabel.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>刻度标签颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='GaugeLabelColor" + [s] + "' option-name='axisLabel.color' data-color='" + series[s].axisLabel.color + "'></div>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#TitleColor" + [s] + "", series[s].title.color, function () {
                    moduleOptionSync(num);
                });
                renderColorpicker("#DetailColor" + [s] + "", series[s].detail.color, function () {
                    moduleOptionSync(num);
                });
                renderColorpicker("#GaugeLabelColor" + [s] + "", series[s].axisLabel.color, function () {
                    moduleOptionSync(num);
                });
            }
            break;
        case 7:
        case 8:
            $(".menu-item[show=18]").find('.layui-collapse').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=18]").find('.layui-collapse').append(
                    "<div class='layui-colla-item'>" +
                    "<h2 class='layui-colla-title'>普通标签样式</h2>" +
                    "<div class='layui-colla-content'>" +
                    "<div class='layui-fluid'>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>普通标签显示</div>" +
                    "<div class='layui-col-md3'>" +
                    "<form class='layui-form'>" +
                    "<input type='checkbox' name='switch' data-check='" + series[s].label.show + "' lay-skin='switch' lay-filter='MenuSwitch' option-name='label.show'>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>普通标签大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].label.fontSize + "' class='dataNumber' option-name='label.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>普通标签颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='TreeLabelColor" + [s] + "' option-name='label.color' data-color='" + series[s].label.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-colla-item'>" +
                    "<h2 class='layui-colla-title'>高亮标签样式</h2>" +
                    "<div class='layui-colla-content'>" +
                    "<div class='layui-fluid'>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>高亮标签显示</div>" +
                    "<div class='layui-col-md3'>" +
                    "<form class='layui-form'>" +
                    "<input type='checkbox' name='switch' data-check='" + series[s].emphasis.label.show + "' lay-skin='switch' lay-filter='MenuSwitch' option-name='emphasis.label.show'>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>普通标签大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].emphasis.label.fontSize + "' class='dataNumber' option-name='emphasis.label.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>高亮标签颜色</div>" +
                    "<div class='layui-col-md3'>" +
                    "<div class='mycolor' id='TreeEmpLabelColor" + [s] + "' option-name='emphasis.label.color' data-color='" + series[s].emphasis.label.color + "'></div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>")
                renderColorpicker("#TreeLabelColor" + [s] + "", series[s].emphasis.label.color, function () {
                    moduleOptionSync(num);
                });
                renderColorpicker("#TreeEmpLabelColor" + [s] + "", series[s].label.color, function () {
                    moduleOptionSync(num);
                });
            }
            break;
        case 9:
        case 13:
        case 12:
        case 11:
        case 10:
            $(".menu-item[show=16]").find('.layui-fluid').empty();
            var series = option.series;
            for (var s in series) {
                $(".menu-item[show=16]").find('.layui-fluid').append(
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'>中心点水平位置</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + ((series[s].center[0]).replace('%', '')) + "' class='dataNumber' option-name='center'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>%</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'>中心点垂直位置</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + ((series[s].center[1]).replace('%', '')) + "' class='dataNumber'  option-name='center'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>%</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>外半径</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + ((series[s].radius[0]).replace('%', '')) + "' class='dataNumber' option-name='radius'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>%</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>内半径</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + ((series[s].radius[1]).replace('%', '')) + "' class='dataNumber'  option-name='radius'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>%</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>显示标签</div>" +
                    "<div class='layui-col-md3'>" +
                    "<form class='layui-form'>" +
                    "<input type='checkbox' name='switch' data-check='" + series[s].label.show + "' lay-skin='switch' lay-filter='MenuSwitch' option-name='label.show'>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5'>标签位置</div>" +
                    "<div class='layui-col-md7'>" +
                    "<select option-name='label.position'>" +
                    "<option value='outside' " + ((series[s].label.position == 'outside') ? 'selected' : ' ') + ">饼图扇区外测</option>" +
                    "<option value='inside' " + ((series[s].label.position == 'inside') ? 'selected' : ' ') + ">饼图扇区内部</option>" +
                    "<option value='center' " + ((series[s].label.position == 'center') ? 'selected' : ' ') + ">饼图中部</option>" +
                    "</select>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'>显示标签引导线</div>" +
                    "<div class='layui-col-md3'>" +
                    "<form class='layui-form'>" +
                    "<input type='checkbox' name='switch' data-check='" + series[s].labelLine.show + "' lay-skin='switch' lay-filter='MenuSwitch' option-name='labelLine.show'>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'>高亮时显示标签</div>" +
                    "<div class='layui-col-md3'>" +
                    "<form class='layui-form'>" +
                    "<input type='checkbox' name='switch' data-check='" + series[s].emphasis.label.show + "' lay-skin='switch' lay-filter='MenuSwitch' option-name='emphasis.label.show'>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "<div class='layui-row'>" +
                    "<div class='layui-col-md5' style='line-height: 18px'>高亮时标签大小</div>" +
                    "<div class='layui-col-md5'>" +
                    "<input type='text' value='" + series[s].emphasis.label.fontSize + "' class='dataNumber' option-name='emphasis.label.fontSize'>" +
                    "</div>" +
                    "<div class='layui-col-md2'>像素</div>" +
                    "</div>")
                var data = series[s].data
                for (var d in data) {
                    $(".menu-item[show=16]").find('.layui-fluid').append(
                        "<div class='layui-row'>" +
                        "<div class='layui-col-md5' style='line-height: 18px'>" + data[d].name + "</div>" +
                        "<div class='layui-col-md3'>" +
                        "<div class='mycolor' id='LineStyleColor" + [d] + "' option-name='lineStyle.color' data-color='" + data[d].itemStyle.color + "'></div>" +
                        "</div>" +
                        "</div>")
                    renderColorpicker("#LineStyleColor" + [d] + "", data[d].itemStyle.color, function () {
                        moduleOptionSync(num);
                    });
                }
            }


            break;
    }
}


function allHide() {
    $('#div4').hide();
    $('#div5').hide();
    $('#div1').hide();
    $('#div2').hide();
    $('#div3').show();
}

$(".postModuleData").on('click',function(){
});

$('.postDatabase').on('click',function(){
    var database = $(".dataMenu .layui-tab-content  .layui-tab-item[data-tag='database'] .dataMenu-input :text").val();
    $.ajax({
        type: "post",
        cache: false,
        data: {"database": database},
        url: ctp + "/module/getModuleDatabase",
        success: function (result) {
            if (result.code == "200") {
                var allData = JSON.stringify(result.data);
                jsoneditor_database.setText(allData);
            } else {
                alert(result.msg);
            }
        },
        error: function () {
            jsoneditor_database.setText({"text": "请求数据库失败..."});
        }
    });
});


