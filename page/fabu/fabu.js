var token = getUrlParam("token");
var code = getUrlParam("code");
var pData = null;
var si = 1;
$(function () {
    showLoading();
    if (!token || !code) {
        goError("403", "参数错误无法访问指定页面!");
    }
    setInterval(getData(), si * 60 * 1000);
});


function getData() {
    $.ajax({
        type: "post",
        url: ctp + "/release/getData",
        data: {"token": token, "code": code},
        success: function (result) {
            closeLoading();
            if (result.code == "200") {
                pData = result.data.panel;
                if (!pData) {
                    goError("403", "参数错误无法访问指定页面!");
                } else {
                    closeLoading();
                    $("title").text(pData.panelName);
                    var option = !pData.configJson ? defaultOption.panel : JSON.parse(pData.configJson);
                    if (option.backgroundType == 0) {
                        //0 背景图
                        $('body').css('background-image', 'url(' + option.backgroundSrc + ')');
                        $('body').css('background-repeat', "no-repeat");
                        if (option.zoomType == 0) {
                            $('body').css('background-size', option.width + "px auto");
                        } else if (option.zoomType == 1) {
                            $('body').css('background-size', "auto " + option.width + "px");
                        } else if (option.zoomType == 2) {
                            $('body').css('background-size', "100% 100%");
                        } else if (option.zoomType === 3) {
                            $('body').css('background-size', option.width + "px " + option.height + "px");
                        }
                    } else {
                        // 1背景色
                        $('body').css('background-color', option.backgroundSrc);
                    }
                    var charts = pData.children;
                    for (var cId in charts) {
                        var chart = charts[cId];
                        if (!chart.configJson || !chart.moduleJson) continue;
                        var pos = JSON.parse(chart.moduleJson);
                        var w = Math.floor($("body").width() * pos.width);
                        var h = Math.floor($("body").height() * pos.height);
                        var l = Math.floor($("body").width() * pos.left);
                        var t = Math.floor($("body").height() * pos.top);
                        var html = '<div class="dragable" id="' + cId + '"  style="position: absolute;left:' +
                            l + 'px; top: ' + t + 'px;  height: ' + h + 'px; width: ' + w + 'px;">' +
                            '<div class="contt" style="height:100%;height:100%;"></div>' +
                            '</div>';
                        $('body').append(html);
                        var echartIns = echarts.init($("#" + cId).children(".contt")[0]);
                        echartIns.setOption(JSON.parse(chart.configJson));
                        charts[cId].echartIns = echartIns;
                    }
                }
                closeLoading();
                $(window).on('resize', function () {
                    $('body').find(".dragable").each(function () {
                        var charts = pData.children;
                        for (var cId in charts) {
                            if (!charts[cId].echartIns) continue;
                            charts[cId].echartIns.resize();
                        }
                    });
                });
                si = result.data.repeatMinute;
            } else {
                goError(result.code, result.msg);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.error("400", "网络请求失败!");
            closeLoading();
        }
    });
}

var defaultOption = {
    panel: {
        width: "1920",
        height: "1080",
        coverSetting: "0", //0自动截图  1手动上传
        backgroundType: "1", //0 背景图 1背景色
        backgroundSrc: "#0d2b5e",
        zoomType: "2" //0等比缩放宽度铺满 1等比缩放高度铺满 2全屏铺满 3原始像素
    }
}


