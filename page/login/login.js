var ctp = "http://localhost:8080" + "/dvm/"

$(function() {
	$('.box').click(function() {
		$('.container').show();
		$('.box').hide();
	})
	$('.x').click(function() {
		$('.container').hide();
		$('.box').show();
	})
})

function userLogin() {
	var loginName = $('#userName').val();
	if (!loginName) {
		alert("请输入账号!");
	}
	var password = $('#password').val();
	if (!password) {
		alert("请输入密码!");
	}
	var parm = {};
	parm.loginName = loginName;
	parm.password = window.btoa(password);

	$.ajax({
		type: "post",
		url: ctp + "/user/login",
		cache: false,
		data: parm,
		async: false,
		success: function(result) {
			if (result.code == "200") {
				//返回成功之后跳入首页 index.html
				localStorage.setItem("data_view_token", result.token);
				localStorage.setItem("data_view_user_name", result.data);
				window.location.href = "/dvs/index.html";
			} else {
				alert(result.msg);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("网络异常");
		}
	});
}
