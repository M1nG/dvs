$(function (){
    qipao();
    zhexian2();
    zhutu();
    bing2();
    bing4();
    ditu3();
})

function qipao() {
    let arr = Array.from(document.querySelectorAll('#qipao1'));
    let charArr = arr.map(item => echarts.init(item))
    // 配置数据
    var qipao1 = {
        width: '100%',
        height: '90%',
        xAxis: {
            axisLine: {
                lineStyle: {
                    color: '#eee',
                    width: 1, //这里是为了突出显示加上的
                }
            },
        },
        yAxis: {
            axisLine: {
                lineStyle: {
                    color: '#eee',
                    width: 1, //这里是为了突出显示加上的
                }
            },
        },
        color:['#00baed'],
        grid: {
            left: '1%',
            top: '10%',
            bottom: '10%',
            right: '1%',
            width: 'auto',
            height: 'auto',
            containLabel: true
        },
        series: [{
            symbolSize: 20,
            data: [
                [10.0, 8.04],
                [8.0, 6.95],
                [13.0, 7.58],
                [9.0, 8.81],
                [11.0, 8.33],
                [14.0, 9.96],
                [6.0, 7.24],
                [4.0, 4.26],
                [12.0, 10.84],
                [7.0, 4.82],
                [5.0, 5.68]
            ],
            type: 'scatter'
        }]
    };
    // 渲染图表
    charArr.forEach(item => item.setOption(qipao1))
}

function zhexian2(){
    let arr = Array.from(document.querySelectorAll('#zhexian2'));
    let charArr = arr.map(item=>echarts.init(item))
    // 配置数据
    var zhexian2 = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                axisLine: {
                    lineStyle: {
                        color: '#eee',
                        width: 1, //这里是为了突出显示加上的
                    }
                },
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: '#eee',
                        width: 1, //这里是为了突出显示加上的
                    }
                },
            }
        ],
        color:['#024186','#02f5fb','#00baed','#0563ba'],
        series: [
            {
                name: '邮件营销',
                type: 'line',
                stack: '总量',
                smooth: 0.7,
                areaStyle: {},
                data: [120, 132, 101, 134, 90, 230, 210],

            },
            {
                name: '联盟广告',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
                name: '视频广告',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
                name: '直接访问',
                type: 'line',
                stack: '总量',
                areaStyle: {},
                data: [320, 332, 301, 334, 390, 330, 320]
            },
            {
                name: '搜索引擎',
                type: 'line',
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top'
                    }
                },
                areaStyle: {},
                data: [820, 932, 901, 934, 1290, 1330, 1320]
            }
        ]
    };
    // 渲染图表
    charArr.forEach(item=>item.setOption(zhexian2))
}

function zhutu() {
    let arr = Array.from(document.querySelectorAll('#zhutu'));
    let charArr = arr.map(item => echarts.init(item))
    // 配置数据
    var zhutu1 = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        color:['#00dfe5','#2388d6'],
        legend: {
            data: ['2011年', '2012年','2013年'],
            align : 'left',
            right:'2%',
            textStyle: {
                color: "#fff"
            }
        },
        grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            data: ['周一', '周二', '周三', '周四', '周五', '周六','周日'],
            axisLine: {
                lineStyle: {
                    color: '#eee',
                    width: 1, //这里是为了突出显示加上的
                }
            },
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, 0.01],
            axisLine: {
                lineStyle: {
                    color: '#eee',
                    width: 1, //这里是为了突出显示加上的
                }
            },

        },
        series: [
            {
                name: '2011年',
                type: 'bar',
                data: [182, 234, 290, 104, 131, 630,440]
            },
            {
                name: '2012年',
                type: 'bar',
                data: [193, 324, 310, 121, 134, 681,520]
            },
            {
                name: '2013年',
                type: 'bar',
                data: [293, 243, 130, 112, 341, 861,426]
            }
        ]
    };
    // 渲染图表
    charArr.forEach(item => item.setOption(zhutu1))
}

function bing2() {

    let arr = Array.from(document.querySelectorAll(".bing2"));
    let charArr = arr.map(item=>echarts.init(item))
    var bing2 = {
        title: {
            subtext: '告警总数',
            text: 489,
            x: 'center',
            y: 'center',
            // 圆弧位置
            center: ['50%', '50%'],
            // 主副标题之间的距离
            itemGap: 1,
            textStyle: {
                color: '#ccc',
                fontSize: 16,
                fontWeight: 'normal'
            },
            subtextStyle: {
                color: '#7f9793',
                fontSize: 10,
            }

        },
        color:['#024186','#02f5fb','#00baed','#0563ba'],
        grid: {
            left: '1%',
            bottom: '10%',
            right: '0',
            containLabel: true
        },
        tooltip: {
            trigger: 'item',
            formatter: '{a} <br/>{b}: {c} ({d}%)'
        },
        series: [
            {
                name: '今日告警占比',
                type: 'pie',
                radius: ['65%', '80%'],
                avoidLabelOverlap: false,
                hoverOffset: 5,
                label: {
                    show: true,

                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '30',
                    }
                },
                labelLine: {
                    show: true
                },
                data: [
                    {value: 81, name: '上塘'},
                    {value: 95, name: '拱宸桥'},
                    {value: 91, name: '小河'},
                    {value: 87, name: '湖墅'},

                ],
            }
        ]
    };
    charArr.forEach(item=>item.setOption(bing2))
}

function bing4() {
    let arr = Array.from(document.querySelectorAll('#bing4'));
    let charArr = arr.map(item => echarts.init(item))
    // 配置数据
    var bing4 = {
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        color:['#024186','#02f5fb','#00baed','#0563ba'],
        series: [{
            name: '面积模式',
            type: 'pie',
            radius: [10, 70],
            label: {
                normal:{
                    show:true
                }
            },
            center: ['50%', '60%'],
            roseType: 'area',
            data: [
                {value: 10, name: 'rose1'},
                {value: 5, name: 'rose2'},
                {value: 15, name: 'rose3'},
                {value: 25, name: 'rose4'},
                {value: 20, name: 'rose5'},
                {value: 35, name: 'rose6'},
                {value: 30, name: 'rose7'},
                {value: 40, name: 'rose8'}
            ]
        }]
    }
    // 渲染图表
    charArr.forEach(item => item.setOption(bing4))
}

function ditu3(){
	//获取杭州市的地图信息
	var arr = Array.from(document.querySelectorAll("#ditu3"));
	var charArr = arr.map(item=>echarts.init(item))
	var geoJson = null;
	var url = "../../lib/json/full.json";
	$.get(url, null, function (ret) {
		debugger
	    geoJson = ret;
	    loadMap(geoJson);
	});
    var dataJson = [
        {name: '上城区', value: 100}, {name: '下城区', value: 450},
        {name: '西湖区', value: 345}, {name: '拱墅区', value: 535},
        {name: '江干区', value: 320}, {name: '滨江区', value: 234},
        {name: '萧山区', value: 189}, {name: '余杭区', value: 99},
        {name: '富阳区', value: 79}, {name: '临安区', value: 190},
        {name: '桐庐县', value: 390}, {name: '淳安县', value: 360},
        {name: '建德市', value: 269},
    ];   
    var loadMap = function (geoJson) {
        echarts.registerMap('tianjin', geoJson);
        var data = dataJson;
        var geoCoordMap = {
            '上城区': [120.171465, 30.215236],
            '下城区': [120.172763, 30.326271],
            '西湖区': [120.027376, 30.132934],
            '拱墅区': [120.204053, 30.374697],
            '江干区': [120.362633, 30.276603],
            '滨江区': [120.198623, 30.166615],
            '萧山区': [120.150693, 29.962932],
            '余杭区': [119.801737, 30.421187],
            '富阳区': [119.949869, 29.849871],
            '临安区': [119.315101, 30.231153],
            '桐庐县': [119.585045, 29.797437],
            '淳安县': [118.624346, 29.404177],
            '建德市': [119.279089, 29.472284],
        }
        var convertData = function (data) {
            var res = [];
            for (var i = 0; i < data.length; i++) {
                var geoCoord = geoCoordMap[data[i].name];
                if (geoCoord) {
                    res.push({
                        name: data[i].name,
                        value: geoCoord.concat(data[i].value)
                    });
                }
            }
            return res;
        };
        // 配置数据
        var ditu3 = {
            tooltip: {
                trigger: 'item',
                formatter: function (params) {
                    if (typeof (params.value)[2] == "undefined") {
                        return params.name + ' : ' + params.value;
                    } else {
                        return params.name + ' : ' + params.value[2];
                    }
                }
            },
            // 导航
            visualMap: {
                show: false,
                type: 'piecewise',
                pieces: [
                    {min: 600},
                    {min: 400, max: 599},
                    {min: 300, max: 399},
                    {min: 200, max: 299},
                    {min: 100, max: 199},
                    {min: 0, max: 99},

                ],
                seriesIndex: [1],
                inRange: {
                    color:['#024186','#02f5fb','#00baed','#0563ba','#0e62aa'],
                }
            },
            geo: {
                show: true,
                map: 'tianjin',
                // 地图位置偏移 左，上
                layoutCenter: ['50%', '50%'],
                // 地图总大小
                layoutSize: '100%',
                label: {
                    normal: {
                        show: false
                    },
                    emphasis: {
                        show: false,
                    }
                },
                itemStyle: {
                    normal: {
                        areaColor: '#031525',
                        borderColor: '#fff',
                        borderWidth: 2,
                    },
                    emphasis: {
                        areaColor: '#56ddff', //鼠标放上去的亮色
                    }
                }
            },

            series: [
                {
                    name: 'credit_pm2.5',
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    data: convertData(data),
                    // 标记大小
                    symbolSize: function (val) {
                        return 5;
                    },
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: true
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fff',
                            areaColor: '#FBE805',
                            // borderColor: '#FBE805',
                        },
                        emphasis: {
                            areaColor: '#FBE805'
                        }
                    },

                },
                {
                    type: 'map',
                    map: 'tianjin',
                    geoIndex: 0,
                    aspectScale: 0.5, //长宽比
                    showLegendSymbol: false, // 存在legend时显示
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false,
                            textStyle: {
                                color: '#fff'
                            }
                        }
                    },
                    // roam: true,
                    itemStyle: {
                        normal: {
                            areaColor: '#031525',
                            borderColor: '#3B5077',
                        },
                        emphasis: {
                            areaColor: '#2B91B7'
                        }
                    },
                    animation: false,
                    data: data
                },
                {
                    name: '点',
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    symbol: 'pin',
                    data: convertData(data),
                    //气泡大小
                    symbolSize: function (val) {

                        return 40;
                    },
                    label: {
                        normal: {
                            show: true,
                            formatter: function (params) {
                                if (typeof (params.value)[2] == "undefined") {
                                    return params.value;
                                } else {
                                    return params.value[2];
                                }
                            },//将集合中序号为2的显示在气泡上，默认为1{x,y,val
                            textStyle: {
                                color: '#fff',//文字颜色
                                fontSize: 9,
                            }
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#2080F7', //标志颜色
                        }
                    },
                    zlevel: 6,

                },
                {
                    name: 'Top 5',
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    data: convertData(data.sort(function (a, b) {
                        return b.value - a.value;
                    }).slice(0, 1)),
                    // 标记大小
                    symbolSize: function (val) {
                        return 5;
                    },
                    showEffectOn: 'render',
                    rippleEffect: {
                        brushType: 'stroke'
                    },
                    hoverAnimation: true,
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#fff',
                            shadowBlur: 10,
                            shadowColor: '#dece00'
                        }
                    },
                    zlevel: 1
                },

            ]
        };
        // 渲染图表
        charArr.forEach(item=>item.setOption(ditu3))
    }
}

