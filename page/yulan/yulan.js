var panelId = getUrlParam("panelId");
var data = window.opener.panelData._toSimpleJSON().panels[panelId];
$(function () {
    showLoading();
    if (!data || data.error) {
        goError("403", "参数错误无法访问指定页面!");
    } else {
        closeLoading();
        $("title").text(data.name);
        var option = !data.option ? defaultOption.panel : data.option;

        if (option.backgroundType == 0) {
            //0 背景图
            $('body').css('background-image', 'url(' + option.backgroundSrc + ')');
            $('body').css('background-repeat', "no-repeat");
            if (option.zoomType == 0) {
                $('body').css('background-size', option.width + "px auto");
            } else if (option.zoomType == 1) {
                $('body').css('background-size', "auto " + option.height + "px");
            } else if (option.zoomType == 2) {
                $('body').css('background-size', "100% 100%");
            } else if (option.zoomType == 3) {
                $('body').css('background-size', option.width + "px " + option.height + "px");
            }
        } else {
            // 1背景色
            $('body').css('background-color', option.backgroundSrc);
        }
        var charts = data.charts;
        for (var cId in charts) {
            var chart = charts[cId];
            if (!chart.offset || !chart.option) continue;
            var pos = chart.offset;
            var w = Math.floor($("body").width() * pos.width);
            var h = Math.floor($("body").height() * pos.height);
            var l = Math.floor($("body").width() * pos.left);
            var t = Math.floor($("body").height() * pos.top);
            var html = '<div class="dragable" id="' + cId + '"  style="position: absolute;left:' +
                l + 'px; top: ' + t + 'px;  height: ' + h + 'px; width: ' + w + 'px;">' +
                '<div class="contt" style="height:100%;height:100%;"></div>' +
                '</div>';
            $('body').append(html);
            var echartIns = echarts.init($("#" + cId).children(".contt")[0]);
            echartIns.setOption(chart.option);
        }
    }
    closeLoading();
    $(window).on('resize', function () {
        $('body').find(".dragable").each(function () {
            var charts = data.charts;
            for (var cId in charts) {
                if (!charts[cId].echartIns) continue;
                charts[cId].echartIns.resize();
            }
        });
    });

});


var defaultOption = {
    panel: {
        width: "1920",
        height: "1080",
        coverSetting: "0", //0自动截图  1手动上传
        backgroundType: "1", //0 背景图 1背景色
        backgroundSrc: "#0d2b5e",
        zoomType: "2" //0等比缩放宽度铺满 1等比缩放高度铺满 2全屏铺满 3原始像素
    }
}